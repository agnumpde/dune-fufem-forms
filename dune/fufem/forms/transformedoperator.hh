// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_TRANSFORMEDOPERATOR_HH
#define DUNE_FUFEM_FORMS_TRANSFORMEDOPERATOR_HH

#include <cstddef>
#include <type_traits>
#include <tuple>
#include <utility>

#include <dune/fufem/forms/localoperators.hh>



namespace Dune::Fufem::Forms {



  template<class Op, class BaseOperator>
  class TransformedOperator : public BaseOperator
  {
    using Transformation = Op;
    using BaseLocalOperator = decltype(localOperator(std::declval<BaseOperator>()));

  public:

    using Element = typename BaseOperator::Element;
    using Domain = typename BaseOperator::Domain;
    using LocalDomain = typename BaseOperator::LocalDomain;
    using Range = decltype(std::declval<Op>()(std::declval<typename BaseOperator::Range>()));

    TransformedOperator(const BaseOperator& baseOperator) :
      BaseOperator(baseOperator),
      transformation_()
    {}

    TransformedOperator(const Transformation& transformation, const BaseOperator& baseOperator) :
      BaseOperator(baseOperator),
      transformation_(transformation)
    {}

    class LocalOperator : public BaseLocalOperator
    {
    public:
      LocalOperator(const Transformation& transformation, BaseLocalOperator&& baseLocalOperator) :
        BaseLocalOperator(std::move(baseLocalOperator)),
        transformation_(transformation)
      {}

      template<class AugmentedLocalDomain>
      auto operator()(const AugmentedLocalDomain& x) const
      {
        if constexpr (BaseOperator::arity == 0)
          return transformation_(BaseLocalOperator::operator()(x));
        else
        {
          auto y = BaseLocalOperator::operator()(x);
          return [=](const auto&... indices) {
            return transformation_(y(indices...));
          };
        }
      }

    private:
      Transformation transformation_;
    };

    friend LocalOperator localOperator(const TransformedOperator& transformedOperator)
    {
      return LocalOperator(transformedOperator.transformation(), localOperator(transformedOperator.baseOperator()));
    }

    friend LocalOperator localFunction(const TransformedOperator& transformedOperator)
    {
      return localOperator(transformedOperator);
    }

    const BaseOperator& baseOperator() const
    {
      return (const BaseOperator&)(*this);
    }

    const Transformation& transformation() const
    {
      return transformation_;
    }

  private:
    Transformation transformation_;
  };



  template<class Op, class F,
    std::enable_if_t<isOperator_v<F>, int> = 0>
  auto compose(const Op& op, const F& f)
  {
    return TransformedOperator(op, f);
  }


  template<class Operator0, class... Operators>
  class SumOperator;

  template<class Op, class... Fi>
  auto compose(const Op& op, const Dune::Fufem::Forms::SumOperator<Fi...>& f)
  {
    return std::apply([&](const auto&...fi) {
        return Dune::Fufem::Forms::SumOperator(compose(op, fi) ...);
    }, f.operators());
  }



  template<class Op>
  class RangeOperator
  {
  public:
    RangeOperator(const Op& op) : op_(op) {}

    template<class F, class... Args,
      std::enable_if_t<isOperatorOrSumOperator_v<F>, int> = 0>
    auto operator()(F&& baseOperator, Args... args) const
    {
      auto captureOp = [op=op_, argTuple = std::tuple(args...)](auto&& arg) {
        return std::apply([&](auto&&... args) {
          return op(std::forward<decltype(arg)>(arg), args...);
        }, argTuple);
      };
      return compose(captureOp, std::forward<F>(baseOperator));
    }

    template<class F,
      std::enable_if_t<isOperatorOrSumOperator_v<F>, int> = 0>
    auto operator()(F&& baseOperator) const
    {
      return compose(op_, std::forward<F>(baseOperator));
    }

    template<class Arg0, class... Args,
      std::enable_if_t<not isOperatorOrSumOperator_v<Arg0>, int> = 0>
    auto operator()(Arg0&& arg0, Args&&... args) const
    {
      return op_(std::forward<Arg0>(arg0), std::forward<Args>(args)...);
    }

  private:
    Op op_;
  };



  template<class F>
  using TransposedOperator = TransformedOperator<Dune::Fufem::Forms::LocalOperators::TransposeOp, F>;

  template<class F>
  auto transpose(const F& f)
  {
    return RangeOperator(Dune::Fufem::Forms::LocalOperators::TransposeOp())(f);
  }



  template<class F>
  using SymmetricedOperator = TransformedOperator<Dune::Fufem::Forms::LocalOperators::SymOp, F>;

  template<class F>
  auto symmetrize(const F& f)
  {
    return RangeOperator(Dune::Fufem::Forms::LocalOperators::SymOp())(f);
  }



  template<class F>
  using TraceOperator = TransformedOperator<Dune::Fufem::Forms::LocalOperators::TraceOp, F>;

  template<class F>
  auto trace(const F& f)
  {
    return RangeOperator(Dune::Fufem::Forms::LocalOperators::TraceOp())(f);
  }





  template<class F,
    std::enable_if_t<isOperatorOrSumOperator_v<F>, int> = 0>
  auto operator- (const F& f)
  {
    return compose([](const auto& x) { return -x; }, f);
  }



  template<class F,
    std::enable_if_t<isOperator_v<F>, int> = 0>
  auto D(const F& f, std::size_t k)
  {
    return compose([k](auto&& g) { return g[k]; }, grad(f));
  }



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_TRANSFORMEDOPERATOR_HH
