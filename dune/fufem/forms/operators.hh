// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_OPERATORS_HH
#define DUNE_FUFEM_FORMS_OPERATORS_HH

#include <cstddef>
#include <type_traits>
#include <utility>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/scaledidmatrix.hh>

#include <dune/fufem/forms/baseclass.hh>
#include <dune/fufem/forms/localoperators.hh>
#include <dune/fufem/forms/transformedoperator.hh>
#include <dune/fufem/forms/unaryoperators.hh>
#include <dune/fufem/forms/productoperator.hh>



namespace Dune::Fufem::Forms {



  // This is the default implementation. The following specializations
  // are optimized versions for certain special cases.
  template<class L, class R,
    std::enable_if_t<isOperatorOrSumOperator_v<L> or isOperatorOrSumOperator_v<R>, int> = 0,
    class = std::void_t<decltype(product(std::declval<LocalOperators::DotOp>(), std::declval<L>(), std::declval<R>()))>>
  auto dot(const L& l, const R& r)
  {
    return product(Dune::Fufem::Forms::LocalOperators::DotOp(), l, r);
  }

  template<class B, class TP, std::size_t argIndex_L, std::size_t argIndex_R>
  auto dot(const FEFunctionOperator<B,TP,argIndex_L>& l, const FEFunctionOperator<B,TP,argIndex_R>& r);

  template<class B, class TP, std::size_t argIndex_L, std::size_t argIndex_R>
  auto dot(const FEFunctionJacobianOperator<B,TP,argIndex_L>& l, const FEFunctionJacobianOperator<B,TP,argIndex_R>& r);

  template<class L, class R>
  auto dot(const TransposedOperator<L>& l, const TransposedOperator<R>& r)
  {
    using namespace Dune::Indices;
    return Dune::Fufem::Forms::dot(l.baseOperator(), r.baseOperator());
  }

  template<class B, class TP, std::size_t argIndex_L, std::size_t argIndex_R>
  auto dot(const FEFunctionOperator<B,TP,argIndex_L>& l, const FEFunctionOperator<B,TP,argIndex_R>& r)
  {
    using Node = typename Dune::TypeTree::ChildForTreePath<typename B::LocalView::Tree, TP>;
    if constexpr (Node::isPower)
    {
      return unpackIntegerSequence(
        [&](auto... i) {
          return (Dune::Fufem::Forms::dot(l.childOperator(i), r.childOperator(i)) + ...);
        },
        std::make_index_sequence<Node::degree()>{});
    }
    else
      return product(Dune::Fufem::Forms::LocalOperators::DotOp(), l, r);
  }

  template<class B, class TP, std::size_t argIndex_L, std::size_t argIndex_R>
  auto dot(const FEFunctionJacobianOperator<B,TP,argIndex_L>& l, const FEFunctionJacobianOperator<B,TP,argIndex_R>& r)
  {
    using Node = typename Dune::TypeTree::ChildForTreePath<typename B::LocalView::Tree, TP>;
    if constexpr (Node::isPower)
    {
      return unpackIntegerSequence(
        [&](auto... i) {
          return (Dune::Fufem::Forms::dot(l.childOperator(i), r.childOperator(i)) + ...);
        },
        std::make_index_sequence<Node::degree()>{});
    }
    else
      return product(Dune::Fufem::Forms::LocalOperators::DotOp(), l, r);
  }



  // This overload is recursively constrained to cases where `operator*` makes sense.
  // Otherwise Dune::dot(a,b) may be activated for operators because it relies on availability
  // of a*b. This is a problem, because it leads to ambiguity with Forms::dot(a,b).
  template<class L, class R,
    std::enable_if_t<isOperatorOrSumOperator_v<L> or isOperatorOrSumOperator_v<R>, int> = 0,
    class = std::void_t<decltype(product(std::declval<LocalOperators::MultOp>(), std::declval<L>(), std::declval<R>()))>>
  auto operator* (const L& l, const R& r)
  {
    return product(LocalOperators::MultOp{}, l, r);
  }



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_OPERATORS_HH
