// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_INTEGRATEDLINEARFORM_HH
#define DUNE_FUFEM_FORMS_INTEGRATEDLINEARFORM_HH

#include <cstddef>
#include <type_traits>
#include <utility>

#include <dune/common/rangeutilities.hh>

#include <dune/typetree/childextraction.hh>

#include <dune/fufem/forms/shapefunctioncache.hh>
#include <dune/fufem/forms/baseclass.hh>
#include <dune/fufem/forms/localsumassembler.hh>



namespace Dune::Fufem::Forms {



  template<class LinearOperator, bool isSemiAffine=true>
  class IntegratedLinearForm
  {
    using TestRootBasis = typename LinearOperator::Basis;
    using TestRootLocalView = typename TestRootBasis::LocalView;
    using TestRootTree = typename TestRootLocalView::Tree;

    using LocalOperator = decltype(localOperator(std::declval<LinearOperator>()));

  public:
    using Element = typename LinearOperator::Element;

    IntegratedLinearForm(const LinearOperator& sumOperator) :
      sumOperator_(sumOperator),
      sumLocalOperator_(localOperator(sumOperator))
    {}

    /**
     * \brief Register local view
     *
     * This has to be called once, before using the assembler.
     * The passed local view must be the same that is used
     integrandOperator calling the assembler for on an element afterwards.
     */
    template<class TestLocalView>
    void preprocess(const TestLocalView& testLocalView)
    {
      cache_.setTree(testLocalView.rootLocalView().tree());
      sumLocalOperator_.bindToLocalView(testLocalView.rootLocalView());
    }

    template<class LocalVector, class TestLocalView>
    void operator()(const Element& element, LocalVector& localVector, const TestLocalView& testSubspaceLocalView)
    {
      const auto& testLocalView = testSubspaceLocalView.rootLocalView();

      const auto& geometry = element.geometry();

      sumLocalOperator_.bind(testLocalView.element());

      Impl::forEachTupleEntry(sumLocalOperator_.operators(), [&](auto& op) {
        auto& cacheForRule = cache_[op.quadratureRuleKey()];
        cacheForRule.invalidate(isSemiAffine);
      });

      Impl::forEachTupleEntry(sumLocalOperator_.operators(), [&](auto& op) {
        auto& cacheForRule = cache_[op.quadratureRuleKey()];

        const auto& testPrefix = op.treePath();
        const auto& testNode = Dune::TypeTree::child(testLocalView.tree(), testPrefix);
        auto& testCache = cacheForRule[testPrefix];

        op.bindToCache(testCache);

        const auto& quad = cacheForRule.rule();

        for (auto k : Dune::range(quad.size()))
        {
          const auto& quadPoint = quad[k];
          const auto integrationWeight = quadPoint.weight() * geometry.integrationElement(quadPoint.position());
          auto evaluatedOperator = op(IndexedReference(quadPoint.position(), k));
          for (auto i : Dune::range(testNode.size()))
            localVector[testNode.localIndex(i)] += evaluatedOperator(i) * integrationWeight;
        }
      });
    }

    const LinearOperator& integrandOperator() const
    {
      return sumOperator_;
    }

  private:
    const LinearOperator sumOperator_;
    LocalOperator sumLocalOperator_;
    ShapeFunctionMultiCache<TestRootTree> cache_;
  };



  template<class LinearOperator, bool isSemiAffine>
  struct IsLocalAssembler<IntegratedLinearForm<LinearOperator, isSemiAffine>> : public std::true_type {};



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_INTEGRATEDLINEARFORM_HH
