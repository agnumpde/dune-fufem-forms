// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_BASECLASS_HH
#define DUNE_FUFEM_FORMS_BASECLASS_HH

#include <cstddef>
#include <type_traits>
#include <tuple>
#include <utility>

#include <dune/common/typetraits.hh>
#include <dune/common/indices.hh>

#include <dune/typetree/childextraction.hh>
#include <dune/typetree/treepath.hh>



namespace Dune::Fufem::Forms::Impl {

  template<class Tuple, class F>
  void forEachTupleEntry(Tuple&& tuple, F&& f){
    std::apply([&](auto&&... entry){
      (f(std::forward<decltype(entry)>(entry)), ...);
    }, tuple);
  }

  // Helper function to derive the leafTreePath associated
  // to a treePath. For treePaths referring to a leaf this is
  // the identity. For treePaths referring to the a power
  // node whose children are leaf, it's the first child.
  // This is used to implement vector valued ansatz functions
  // for power nodes.
  template<class Tree, class TreePath>
  static auto leafTreePath(const TreePath& tp)
  {
    using Node = typename Dune::TypeTree::ChildForTreePath<Tree, TreePath>;
    if constexpr(Node::isLeaf)
      return tp;
    else if constexpr(Node::isPower and Node::ChildType::isLeaf)
      return Dune::TypeTree::push_back(tp, Dune::Indices::_0);
  }

  template<class Tree, class TreePath>
  using LeafTreePath = decltype(leafTreePath<Tree>(std::declval<TreePath>()));

} // namespace Impl



namespace Dune::Fufem::Forms {



  // Base class of operator implementations. This is motivated by UFL.
  // All classes deriving from this are multilinear
  // maps from finite element spaces into the space of functions
  // from the domain into some finite dimensional vector spaces.
  // Poinwise products (scalar-vector, matrix-vector, dot,...) of
  // multilinear maps induces multilinear maps of higher order.
  // The product of a k-linear map and an l-linear is a (k+l) linear
  // map into a suitable function space.
  //
  // Example:
  // Denote by {A->B} the set of all functions f:B->A. Now let
  // D a domain in R^d, V \subset {D->R} and W \subset {D->R}
  // two FE-spaces, and f \in {D->R^(d,d)} a fixed matrix valued
  // function.
  //
  // Then f is a 0-linear map into {D->R^(d,d)}.
  // The gradient operator \nabla:V -> {D->R^d} is a 1-linear map into {D->R^d}.
  // The gradient operator \nabla:W -> {D->R^d} is a 1-linear map into {D->R^d}.
  // The pointwise matrix-vector product f\nabla:V -> {D->R^d} is a 1-linear map into {D->R^d}.
  // The pointwise dot product dot(f\nabla(.), \nabla(.)):V\times W -> {D->R}
  // is a 2-linear map into {D->R}.
  //
  // Integrating a k-linear operator F:V_1 \times ... \times V_k -> {D->R} over D
  // results in a k-linear form \int_D F(...)dx : V_1 \times ... \times V_k -> D
  template<std::size_t k>
  class MultilinearOperator {
  public:
    static constexpr std::size_t arity = k;
  };

  // Forward declaration
  template<class Operator, class... Operators>
  class SumOperator;



namespace Impl {

  std::false_type isOperatorHelper(const void*)
  {
    return {};
  }

  template<std::size_t k>
  std::true_type isOperatorHelper(const MultilinearOperator<k>*)
  {
    return {};
  }

  template<class T>
  struct IsSumOperatorHelper : std::false_type {};

  template<class... F>
  struct IsSumOperatorHelper<SumOperator<F...>> : std::true_type {};

}


  // Check if a class is a MultilinearOperator

  template<class F>
  using IsOperator = decltype(Impl::isOperatorHelper(std::declval<const std::decay_t<F>*>()));

  template<class F>
  inline constexpr bool isOperator_v = IsOperator<F>::value;



  template<class F>
  using isSumOperator = Impl::IsSumOperatorHelper<std::decay_t<F>>;

  template<class F>
  inline constexpr bool isSumOperator_v = isSumOperator<F>::value;



  template<class F>
  using IsOperatorOrSumOperator = std::disjunction<IsOperator<F>, isSumOperator<F>>;

  template<class F>
  inline constexpr bool isOperatorOrSumOperator_v = IsOperatorOrSumOperator<F>::value;



  template<class T, class Index>
  class IndexedReference
  {
  public:
    IndexedReference(const T& ref, const Index& index) :
      ref_(&ref),
      index_(index)
    {}

    operator const T&() const
    {
      return *ref_;
    }

    const Index& index() const
    {
      return index_;
    }

  private:
    const T* ref_;
    const Index& index_;
  };



} // namespace Dune::Fufem::Forms



#endif // DUNE_FUFEM_FORMS_BASECLASS_HH
