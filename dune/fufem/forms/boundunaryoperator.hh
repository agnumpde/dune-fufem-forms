// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_BOUNDUNARYOPERATOR_HH
#define DUNE_FUFEM_FORMS_BOUNDUNARYOPERATOR_HH

#include <cstddef>
#include <type_traits>
#include <tuple>
#include <utility>

#include <dune/fufem/forms/baseclass.hh>
#include <dune/fufem/forms/transformedoperator.hh>
#include <dune/fufem/forms/sumoperator.hh>



namespace Dune::Fufem::Forms {


  template<class F, class V>
  class BoundUnaryOperator : public MultilinearOperator<0>
  {
    using UnaryOperator = F;
    using LocalUnaryOperator = decltype(localOperator(std::declval<UnaryOperator>()));

  public:

    // Types present in DiscreteGlobalBasisFunctionBase
    using Basis = typename UnaryOperator::Basis;
    using Vector = V;
    using Coefficient = Dune::AutonomousValue<decltype(std::declval<Vector>()[std::declval<typename Basis::MultiIndex>()])>;
    using GridView = typename Basis::GridView;
    using EntitySet = typename Dune::Functions::GridViewEntitySet<GridView, 0>;
    using Domain = typename EntitySet::GlobalCoordinate;
    using LocalDomain = typename EntitySet::LocalCoordinate;
    using Element = typename EntitySet::Element;

    // Types required by Operator interface
    using Range = typename UnaryOperator::Range;

    BoundUnaryOperator(const UnaryOperator& unaryOperator, const Vector& coefficients) :
      unaryOperator_(unaryOperator),
      coefficients_(Dune::stackobject_to_shared_ptr(coefficients))
    {
      static_assert(isOperatorOrSumOperator_v<UnaryOperator>, "The type passed to BoundUnaryOperator is not a operator.");
      static_assert(UnaryOperator::arity==1, "The operator passed to BoundUnaryOperator is not unary.");
    }

    BoundUnaryOperator(const UnaryOperator& unaryOperator, Vector&& coefficients) :
      unaryOperator_(unaryOperator),
      coefficients_(std::make_shared<const Vector>(std::move(coefficients)))
    {
      static_assert(isOperatorOrSumOperator_v<UnaryOperator>, "The type passed to BoundUnaryOperator is not a operator.");
      static_assert(UnaryOperator::arity==1, "The operator passed to BoundUnaryOperator is not unary.");
    }

    BoundUnaryOperator(const UnaryOperator& unaryOperator, std::shared_ptr<const Vector> coefficients) :
      unaryOperator_(unaryOperator),
      coefficients_(std::move(coefficients))
    {
      static_assert(isOperatorOrSumOperator_v<UnaryOperator>, "The type passed to BoundUnaryOperator is not a operator.");
      static_assert(UnaryOperator::arity==1, "The operator passed to BoundUnaryOperator is not unary.");
    }

    class LocalOperator
    {
      using LocalView = typename Basis::LocalView;
      using Tree = typename LocalView::Tree;
      using size_type = typename Basis::LocalView::Tree::size_type;
      using Cache = ShapeFunctionCache<Tree>;
      using QuadratureRule = typename Cache::QuadratureRule;
      using QuadraturePoint = typename QuadratureRule::value_type;

    public:

      using Domain = LocalDomain;
      using Element = typename EntitySet::Element;

      LocalOperator(LocalUnaryOperator&& localUnaryOperator, const Basis& basis, std::shared_ptr<const Vector> coefficients) :
        localUnaryOperator_(std::move(localUnaryOperator)),
        localView_(basis.localView()),
        cache_(),
        coefficients_(std::move(coefficients))
      {
        localDoFs_.reserve(localView_.maxSize());
        localUnaryOperator_.bindToLocalView(localView_),
        rule_.emplace_back(typename QuadraturePoint::Vector(),0.0);
        cache_.setTree(localView_.tree());
        cache_.setRule(rule_);
      }

      LocalOperator(const LocalOperator& other) :
        LocalOperator(LocalUnaryOperator(other.localUnaryOperator_), other.localView_.globalBasis(), other.coefficients_)
      {}

      // Local Function interface

      void bind(const Element& element)
      {
        localView_.bind(element);
        localUnaryOperator_.bind(element);

        // Cache dofs associated to tree of unary operator
        auto cacheOperatorDOFs = [&](auto& op) {
          const auto& treePath = op.treePath();
          const auto& node = Dune::TypeTree::child(localView_.tree(), treePath);
          for (size_type i = 0; i < node.size(); ++i)
          {
            auto localIndex = node.localIndex(i);
            localDoFs_[localIndex] = (*coefficients_)[localView_.index(localIndex)];
          }
        };

        if constexpr (isOperator_v<UnaryOperator>)
          cacheOperatorDOFs(localUnaryOperator_);
        else if constexpr (isSumOperator_v<UnaryOperator>)
          Impl::forEachTupleEntry(localUnaryOperator_.operators(), cacheOperatorDOFs);
      }

      //! Unbind the local-function.
      void unbind()
      {
        localView_.unbind();
      }

      //! Check if LocalFunction is already bound to an element.
      bool bound() const
      {
        return localView_.bound();
      }

      auto operator()(const LocalDomain& x) const
      {
        std::size_t dummyIndex=0;
        rule_[0] = QuadraturePoint{x, 0};
        cache_.invalidate(false);
        Range y;
        y = 0;

        auto accumulateOperatorValues = [&](auto& op) {
          const auto& treePath = op.treePath();
          const auto& node = Dune::TypeTree::child(localView_.tree(), treePath);
          op.bindToCache(cache_[treePath]);
          auto evaluatedOperator = op(IndexedReference(x, dummyIndex));
          for (size_type i = 0; i < node.size(); ++i)
            y += localDoFs_[node.localIndex(i)] * evaluatedOperator(i);
        };

        if constexpr (isOperator_v<UnaryOperator>)
          accumulateOperatorValues(localUnaryOperator_);
        else if constexpr (isSumOperator_v<UnaryOperator>)
          Impl::forEachTupleEntry(localUnaryOperator_.operators(), accumulateOperatorValues);

        return y;
      }

      //! Return the element the local-function is bound to.
      const Element& localContext() const
      {
        return localView_.element();
      }

      // Additional interface of local nullary operator
      auto quadratureRuleKey() const
      {
        return localUnaryOperator_.quadratureRuleKey();
      }

    private:
      mutable LocalUnaryOperator localUnaryOperator_;
      LocalView localView_;
      mutable QuadratureRule rule_;
      mutable Cache cache_;
      std::shared_ptr<const Vector> coefficients_;
      std::vector<Coefficient> localDoFs_;
    };

    friend LocalOperator localOperator(const BoundUnaryOperator& boundUnaryOperator)
    {
      return LocalOperator(localOperator(boundUnaryOperator.unaryOperator_), boundUnaryOperator.unaryOperator_.basis(), boundUnaryOperator.coefficients_);
    }

    friend LocalOperator localFunction(const BoundUnaryOperator& boundUnaryOperator)
    {
      return localOperator(boundUnaryOperator);
    }

    friend auto jacobian(const BoundUnaryOperator& f)
    {
      static_assert(isOperator_v<UnaryOperator>, "jacobian(BoundUnaryOperator<SumOperator<...>,V>(...)) is not implemented.");
      return Dune::Fufem::Forms::BoundUnaryOperator(jacobian(f.unaryOperator_), f.coefficients_);
    }

    friend auto gradient(const BoundUnaryOperator& f)
    {
      static_assert(isOperator_v<UnaryOperator>, "gradient(BoundUnaryOperator<SumOperator<...>,V>(...)) is not implemented.");
      return Dune::Fufem::Forms::BoundUnaryOperator(gradient(f.unaryOperator_), f.coefficients_);
    }

    friend auto grad(const BoundUnaryOperator& f)
    {
      static_assert(isOperator_v<UnaryOperator>, "grad(BoundUnaryOperator<SumOperator<...>,V>(...)) is not implemented.");
      return Dune::Fufem::Forms::BoundUnaryOperator(grad(f.unaryOperator_), f.coefficients_);
    }

    friend auto divergence(const BoundUnaryOperator& f)
    {
      static_assert(isOperator_v<UnaryOperator>, "divergence(BoundUnaryOperator<SumOperator<...>,V>(...)) is not implemented.");
      return Dune::Fufem::Forms::BoundUnaryOperator(divergence(f.unaryOperator_), f.coefficients_);
    }

    friend auto div(const BoundUnaryOperator& f)
    {
      static_assert(isOperator_v<UnaryOperator>, "div(BoundUnaryOperator<SumOperator<...>,V>(...)) is not implemented.");
      return Dune::Fufem::Forms::BoundUnaryOperator(div(f.unaryOperator_), f.coefficients_);
    }

  private:
    UnaryOperator unaryOperator_;
    std::shared_ptr<const Vector> coefficients_;
  };



  template<class F, class V,
    std::enable_if_t<isOperatorOrSumOperator_v<F>, int> = 0>
  auto bindToCoefficients(const F& f, V&& vector)
  {
    using Basis = typename F::Basis;

    // Small helper functions to wrap vectors using istlVectorBackend
    // if they do not already satisfy the VectorBackend interface.
    auto toConstVectorBackend = [&](auto&& v) -> decltype(auto) {
      if constexpr (models<Dune::Functions::Concept::ConstVectorBackend<Basis>, decltype(v)>()) {
        return std::forward<decltype(v)>(v);
      } else {
        return Dune::Functions::istlVectorBackend(v);
      }
    };

    return BoundUnaryOperator(f, toConstVectorBackend(std::forward<V>(vector)));
  }

//  template<class F, class V,
//    std::enable_if_t<isOperatorOrSumOperator_v<F>, int> = 0,
//    std::enable_if_t<not isOperatorOrSumOperator_v<std::decay_t<V>>, int> = 0>
//  auto operator,(const F& f, V&& vector)
//  {
//    return bindToCoefficients(f, std::forward<V>(vector));
//  }


} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_BOUNDUNARYOPERATOR_HH
