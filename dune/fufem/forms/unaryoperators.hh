// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_UNARYOPERATORS_HH
#define DUNE_FUFEM_FORMS_UNARYOPERATORS_HH

#include <cstddef>
#include <type_traits>
#include <vector>

#include <dune/common/typetraits.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/indices.hh>

#include <dune/typetree/childextraction.hh>
#include <dune/typetree/treepath.hh>

#include <dune/functions/functionspacebases/subspacebasis.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

#include <dune/fufem/forms/baseclass.hh>
#include <dune/fufem/forms/transformedoperator.hh>


namespace Dune::Fufem::Forms {



  template<std::size_t k>
  struct UnaryOperator : public MultilinearOperator<1>
  {
    static constexpr std::size_t argIndex = k;
  };



  // Forward declarations

  template<class B, class TP, std::size_t argIndex>
  class FEFunctionJacobianOperator;

  template<class B, class TP, std::size_t argIndex>
  class FEFunctionDivergenceOperator;

  template<class B, class TP, std::size_t argIndex>
  class FEFunctionOperator;



  // Linear map on FE-space that maps v onto itself
  template<class B, class TP, std::size_t argIndex>
  class FEFunctionOperator : public UnaryOperator<argIndex>
  {
  private:

    using LocalView = typename B::LocalView;
    using Tree = typename LocalView::Tree;
    using Node = typename Dune::TypeTree::ChildForTreePath<Tree, TP>;
    using LeafNode = typename Dune::TypeTree::ChildForTreePath<Tree, Impl::LeafTreePath<Tree,TP>>;

    // Helper function to simplify generation of Range type
    static constexpr auto generateRangeType()
    {
      using LeafFERange = typename LeafNode::FiniteElement::Traits::LocalBasisType::Traits::RangeType;
      using LeafFERangeField = typename LeafNode::FiniteElement::Traits::LocalBasisType::Traits::RangeFieldType;
      if constexpr(Node::isLeaf)
        return Dune::MetaType<LeafFERange>();
      else if constexpr(Node::isPower and Node::ChildType::isLeaf)
        return Dune::MetaType<Dune::FieldVector<LeafFERangeField,Node::degree()>>();
    }

  public:

    using Basis = B;
    using TreePath = TP;
    using Element = typename Basis::LocalView::Element;
    using Domain = typename Element::Geometry::GlobalCoordinate;
    using LocalDomain = typename Element::Geometry::LocalCoordinate;
    using Range = typename decltype(generateRangeType())::type;

    FEFunctionOperator(const Basis& basis, const TreePath& treePath) :
      basis_(basis),
      treePath_(treePath)
    {}

    class LocalOperator
    {
      using Cache = ShapeFunctionCache<Node>;
      using ValueCache = typename ShapeFunctionCache<LeafNode>::ValueCache;

    public:
      LocalOperator(const TreePath treePath) :
        treePath_(treePath),
        leafTreePath_(Impl::leafTreePath<Tree>(treePath)),
        leafNode_(nullptr),
        valueCache_(nullptr)
      {}

      void bind(const Element&)
      {
        quadratureRuleKey_ = QuadratureRuleKey(leafNode_->finiteElement());
      }

      void unbind()
      {}

      auto quadratureRuleKey() const
      {
        return quadratureRuleKey_;
      }

      void bindToLocalView(const LocalView& localView)
      {
        leafNode_ = & Dune::TypeTree::child(localView.tree(), leafTreePath_);
      }

      void bindToCache(Cache& cache)
      {
        if constexpr(Node::isLeaf)
          valueCache_ = &(cache.getValues());
        else if constexpr(Node::isPower and Node::ChildType::isLeaf)
          valueCache_ = &(cache[0].getValues());
      }

      const auto& treePath() const
      {
        return treePath_;
      }

      template<class AugmentedLocalDomain>
      auto operator()(const AugmentedLocalDomain& x) const
      {
        const auto& values = (*valueCache_)[x.index()];
        if constexpr(Node::isLeaf)
          return [&](const auto& i) {
            return values[i];
          };
        else if constexpr(Node::isPower and Node::ChildType::isLeaf)
          return [&](const auto& i) {
            auto result = Range(0);
            auto componentIndex = i / leafNode_->size();
            auto shapeFunctionIndex = i % leafNode_->size();
            result[componentIndex] = values[shapeFunctionIndex];
            return result;
          };
      }

    private:
      const TreePath treePath_;
      const Impl::LeafTreePath<Tree,TreePath> leafTreePath_;
      const LeafNode* leafNode_;
      const ValueCache* valueCache_;
      QuadratureRuleKey quadratureRuleKey_;
    };

    friend LocalOperator localOperator(const FEFunctionOperator& op)
    {
      return LocalOperator(op.treePath());
    }

    const auto& basis() const
    {
      return basis_;
    }

    const auto& treePath() const
    {
      return treePath_;
    }

    friend auto jacobian(const FEFunctionOperator& f)
    {
      return FEFunctionJacobianOperator<Basis, TreePath, argIndex>(f.basis_, f.treePath_);
    }

    friend auto gradient(const FEFunctionOperator& f)
    {
      return Fufem::Forms::transpose(jacobian(f));
    }

    friend auto grad(const FEFunctionOperator& f)
    {
      return Fufem::Forms::transpose(jacobian(f));
    }

    friend auto divergence(const FEFunctionOperator& f)
    {
      return FEFunctionDivergenceOperator<Basis, TreePath, argIndex>(f.basis_, f.treePath_);
    }

    friend auto div(const FEFunctionOperator& f)
    {
      return divergence(f);
    }

    template<std::size_t i>
    auto childOperator(Dune::index_constant<i> childIndex) const
    {
      auto childTP = Dune::TypeTree::push_back(treePath_, childIndex);
      return FEFunctionOperator<Basis, decltype(childTP), argIndex>(basis_, childTP);
    }

  private:
    const Basis& basis_;
    const TreePath treePath_;
  };



  // Linear map on FE-space that maps v onto its Jacobian
  template<class B, class TP, std::size_t argIndex>
  class FEFunctionJacobianOperator : public UnaryOperator<argIndex>
  {
  private:

    using LocalView = typename B::LocalView;
    using Tree = typename LocalView::Tree;
    using Node = typename Dune::TypeTree::ChildForTreePath<Tree, TP>;
    using LeafNode = typename Dune::TypeTree::ChildForTreePath<Tree, Impl::LeafTreePath<Tree,TP>>;

    // Helper function to simplify generation of Range type
    static constexpr auto generateRangeType()
    {
      using LeafFEJacobian = typename LeafNode::FiniteElement::Traits::LocalBasisType::Traits::JacobianType;
      using LeafFERangeField = typename LeafNode::FiniteElement::Traits::LocalBasisType::Traits::RangeFieldType;
      if constexpr(Node::isLeaf)
        return Dune::MetaType<LeafFEJacobian>();
      else if constexpr(Node::isPower and Node::ChildType::isLeaf)
      {
        constexpr auto components = Node::degree();
        constexpr auto dimension = LeafNode::FiniteElement::Traits::LocalBasisType::Traits::dimDomain;
        return Dune::MetaType<Dune::FieldMatrix<LeafFERangeField,components,dimension>>();
      }
    }

  public:

    using Basis = B;
    using TreePath = TP;
    using Element = typename Basis::LocalView::Element;
    using Domain = typename Element::Geometry::GlobalCoordinate;
    using LocalDomain = typename Element::Geometry::LocalCoordinate;
    using Range = typename decltype(generateRangeType())::type;

    FEFunctionJacobianOperator(const Basis& basis, const TreePath& treePath) :
      basis_(basis),
      treePath_(treePath)
    {}

    class LocalOperator
    {
      using Cache = ShapeFunctionCache<Node>;
      using GlobalJacobianCache = typename ShapeFunctionCache<LeafNode>::GlobalJacobianCache;

    public:
      LocalOperator(const TreePath& treePath) :
        treePath_(treePath),
        leafTreePath_(Impl::leafTreePath<Tree>(treePath)),
        leafNode_(nullptr),
        globalJacobianCache_(nullptr)
      {}

      void bind(const Element&)
      {
        quadratureRuleKey_ = QuadratureRuleKey(leafNode_->finiteElement()).derivative();
      }

      void unbind()
      {}

      auto quadratureRuleKey() const
      {
        return quadratureRuleKey_;
      }

      void bindToLocalView(const LocalView& localView)
      {
        leafNode_ = & Dune::TypeTree::child(localView.tree(), leafTreePath_);
      }

      void bindToCache(Cache& cache)
      {
        if constexpr(Node::isLeaf)
          globalJacobianCache_ = &(cache.getGlobalJacobians());
        else if constexpr(Node::isPower and Node::ChildType::isLeaf)
          globalJacobianCache_ = &(cache[0].getGlobalJacobians());
      }

      const auto& treePath() const
      {
        return treePath_;
      }

      template<class AugmentedLocalDomain>
      auto operator()(const AugmentedLocalDomain& x) const
      {
        const auto& globalJacobians = (*globalJacobianCache_)[x.index()];
        if constexpr(Node::isLeaf)
          return [&](const auto& i) {
            return globalJacobians[i];
          };
        else if constexpr(Node::isPower and Node::ChildType::isLeaf)
          return [&](const auto& i) {
            auto result = Range(0);
            auto componentIndex = i / leafNode_->size();
            auto shapeFunctionIndex = i % leafNode_->size();
            result[componentIndex] = globalJacobians[shapeFunctionIndex][0];
            return result;
          };
      }

    private:
      const TreePath treePath_;
      const Impl::LeafTreePath<Tree,TreePath> leafTreePath_;
      const LeafNode* leafNode_;
      const GlobalJacobianCache* globalJacobianCache_;
      QuadratureRuleKey quadratureRuleKey_;
    };

    friend LocalOperator localOperator(const FEFunctionJacobianOperator& op)
    {
      return LocalOperator(op.treePath());
    }

    const auto& basis() const
    {
      return basis_;
    }

    const auto& treePath() const
    {
      return treePath_;
    }

    template<std::size_t i>
    auto childOperator(Dune::index_constant<i> childIndex) const
    {
      auto childTP = Dune::TypeTree::push_back(treePath_, childIndex);
      return FEFunctionJacobianOperator<Basis, decltype(childTP), argIndex>(basis_, childTP);
    }

  private:
    const Basis& basis_;
    const TreePath treePath_;
  };



  // Linear map on FE-space that maps v onto its divergence
  template<class B, class TP, std::size_t argIndex>
  class FEFunctionDivergenceOperator : public UnaryOperator<argIndex>
  {
  private:

    using LocalView = typename B::LocalView;
    using Tree = typename LocalView::Tree;
    using Node = typename Dune::TypeTree::ChildForTreePath<Tree, TP>;
    using LeafNode = typename Dune::TypeTree::ChildForTreePath<Tree, Impl::LeafTreePath<Tree,TP>>;

    // Helper function to simplify generation of Range type
    static constexpr auto generateRangeType()
    {
      using LeafFERangeField = typename LeafNode::FiniteElement::Traits::LocalBasisType::Traits::RangeFieldType;
      using LeafFEJacobian = typename LeafNode::FiniteElement::Traits::LocalBasisType::Traits::JacobianType;
      if constexpr(Node::isLeaf)
        return Dune::MetaType<LeafFERangeField>();
      else if constexpr(Node::isPower and Node::ChildType::isLeaf)
        return Dune::MetaType<typename LeafFEJacobian::block_type>();
    }

  public:

    using Basis = B;
    using TreePath = TP;
    using Element = typename Basis::LocalView::Element;
    using Domain = typename Element::Geometry::GlobalCoordinate;
    using LocalDomain = typename Element::Geometry::LocalCoordinate;
    using Range = typename decltype(generateRangeType())::type;

    FEFunctionDivergenceOperator(const Basis& basis, const TreePath& treePath) :
      basis_(basis),
      treePath_(treePath)
    {}

    class LocalOperator
    {
      using Cache = ShapeFunctionCache<Node>;
      using GlobalJacobianCache = typename ShapeFunctionCache<LeafNode>::GlobalJacobianCache;

    public:
      LocalOperator(const TreePath& treePath) :
        treePath_(treePath),
        leafTreePath_(Impl::leafTreePath<Tree>(treePath)),
        leafNode_(nullptr),
        globalJacobianCache_(nullptr)
      {}

      void bind(const Element&)
      {
        quadratureRuleKey_ = QuadratureRuleKey(leafNode_->finiteElement()).derivative();
      }

      void unbind()
      {}

      auto quadratureRuleKey() const
      {
        return quadratureRuleKey_;
      }

      void bindToLocalView(const LocalView& localView)
      {
        leafNode_ = & Dune::TypeTree::child(localView.tree(), leafTreePath_);
      }

      void bindToCache(Cache& cache)
      {
        if constexpr(Node::isLeaf)
          globalJacobianCache_ = &(cache.getGlobalJacobians());
        else if constexpr(Node::isPower and Node::ChildType::isLeaf)
          globalJacobianCache_ = &(cache[0].getGlobalJacobians());
      }

      const auto& treePath() const
      {
        return treePath_;
      }

      template<class AugmentedLocalDomain>
      auto operator()(const AugmentedLocalDomain& x) const
      {
        const auto& globalJacobians = (*globalJacobianCache_)[x.index()];
        if constexpr(Node::isLeaf)
          return [&](const auto& i) {
            using Field = typename LeafNode::FiniteElement::Traits::LocalBasisType::Traits::RangeFieldType;
            static constexpr auto dimension = LeafNode::FiniteElement::Traits::LocalBasisType::Traits::dimDomain;
            auto div = Field{0};
            for(std::size_t j=0; j<dimension; ++j)
              div += globalJacobians[i][j][j];
            return div;
          };
        else if constexpr(Node::isPower and Node::ChildType::isLeaf)
          return [&](const auto& i) {
            // Here we should compute the trace of the global Jacobian J
            // that we would need to build first. But since only the
            // componentIndex'th row of J is nonzero, we can simply
            // return the diagonal entry of this row which coincides
            // with the componentIndex'th entry of the gradient of
            // the respective scalar basis function.
            auto componentIndex = i / leafNode_->size();
            auto shapeFunctionIndex = i % leafNode_->size();
            return globalJacobians[shapeFunctionIndex][0][componentIndex];
          };
      }

    private:
      const TreePath treePath_;
      const Impl::LeafTreePath<Tree,TreePath> leafTreePath_;
      const LeafNode* leafNode_;
      const GlobalJacobianCache* globalJacobianCache_;
      QuadratureRuleKey quadratureRuleKey_;
    };

    friend LocalOperator localOperator(const FEFunctionDivergenceOperator& op)
    {
      return LocalOperator(op.treePath());
    }

    const auto& basis() const
    {
      return basis_;
    }

    const auto& treePath() const
    {
      return treePath_;
    }

  private:
    const Basis& basis_;
    const TreePath treePath_;
  };




  template<class Basis>
  auto testFunction(const Basis& basis)
  {
    using RootBasis = std::decay_t<decltype(basis.rootBasis())>;
    return FEFunctionOperator<RootBasis, typename Basis::PrefixPath, 0>(basis.rootBasis(), basis.prefixPath());
  }

  template<class Basis, class... Args, std::enable_if_t<(sizeof...(Args)>0), int> = 0>
  auto testFunction(const Basis& basis, Args&&... args)
  {
    return testFunction(Dune::Functions::subspaceBasis(basis, args...));
  }




  template<class Basis>
  auto trialFunction(const Basis& basis)
  {
    using RootBasis = std::decay_t<decltype(basis.rootBasis())>;
    return FEFunctionOperator<RootBasis, typename Basis::PrefixPath, 1>(basis.rootBasis(), basis.prefixPath());
  }

  template<class Basis, class... Args, std::enable_if_t<(sizeof...(Args)>0), int> = 0>
  auto trialFunction(const Basis& basis, Args&&... args)
  {
    return trialFunction(Dune::Functions::subspaceBasis(basis, args...));
  }



} // namespace Dune::Fufem::Forms



#endif // DUNE_FUFEM_FORMS_UNARYOPERATORS_HH
