// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_LOCALOPERATORS_HH
#define DUNE_FUFEM_FORMS_LOCALOPERATORS_HH

#include <cstddef>
#include <type_traits>
#include <utility>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/scaledidmatrix.hh>



namespace Dune::Fufem::Forms::LocalOperators {



  struct DotOp {

    template<class K1, class K2, int n>
    auto operator()(const Dune::FieldVector<K1,n>& x, const Dune::FieldVector<K2,n>& y) const
    {
      using K = typename PromotionTraits<K1,K2>::PromotedType;
      auto result = K(0);
      for(std::size_t i=0; i<n; ++i)
        result += x[i]*y[i];
      return result;
    }

    template<class K1, class K2, int n, int m>
    auto operator()(const Dune::FieldMatrix<K1,n,m>& x, const Dune::FieldMatrix<K2,n,m>& y) const
    {
      using K = typename PromotionTraits<K1,K2>::PromotedType;
      auto result = K(0);
      for(std::size_t i=0; i<n; ++i)
        for(std::size_t j=0; j<m; ++j)
          result += x[i][j]*y[i][j];
      return result;
    }

    template<class K1, class K2, int n>
    auto operator()(const Dune::FieldMatrix<K1,n,1>& x, const Dune::FieldVector<K2,n>& y) const
    {
      using K = typename PromotionTraits<K1,K2>::PromotedType;
      auto result = K(0);
      for(std::size_t i=0; i<n; ++i)
        result += x[i][0]*y[i];
      return result;
    }

    template<class K1, class K2, int n>
    auto operator()(const Dune::FieldVector<K1,n>& x, const Dune::FieldMatrix<K2,n,1>& y) const
    {
      using K = typename PromotionTraits<K1,K2>::PromotedType;
      auto result = K(0);
      for(std::size_t i=0; i<n; ++i)
        result += x[i]*y[i][0];
      return result;
    }

    template<class K1, class K2, int n>
    auto operator()(const Dune::ScaledIdentityMatrix<K1,n>& x, const Dune::FieldMatrix<K2,n,n>& y) const
    {
      using K = typename PromotionTraits<K1,K2>::PromotedType;
      auto result = K(0);
      for(std::size_t i=0; i<x.N(); ++i)
        result += y[i][i];
      return x.scalar()*result;
    }

    template<class K1, class K2, int n>
    auto operator()(const Dune::FieldMatrix<K1,n,n>& x, const Dune::ScaledIdentityMatrix<K2,n>& y) const
    {
      using K = typename PromotionTraits<K1,K2>::PromotedType;
      auto result = K(0);
      for(std::size_t i=0; i<x.N(); ++i)
        result += x[i][i];
      return result*y.scalar();
    }

    template<class K1, class K2, int n>
    auto operator()(const Dune::ScaledIdentityMatrix<K1,n>& x, const Dune::ScaledIdentityMatrix<K2,n>& y) const
    {
      return n*x.scalar()*y.scalar();
    }

  };

  struct MultOp {

    template<class X, class Y,
      class = std::void_t<decltype(std::declval<X>()*std::declval<Y>())>>
    auto operator()(const X& x, const Y& y) const
    {
      return x*y;
    }

    template<class K1, class K2, int n, int m>
    auto operator()(const Dune::FieldMatrix<K1,n,m>& x, const Dune::FieldVector<K2,m>& y) const
    {
      using K = typename PromotionTraits<K1,K2>::PromotedType;
      Dune::FieldVector<K,n> result;
      x.mv(y, result);
      return result;
    }

    template<class K, class Y>
    auto operator()(const Dune::FieldMatrix<K,1,1>& x, const Y& y) const
    {
      return x[0][0]*y;
    }

    template<class K, class Y>
    auto operator()(const Dune::FieldVector<K,1>& x, const Y& y) const
    {
      return x[0]*y;
    }

  };



  struct TransposeOp {
    template<class K, int n, int m>
    auto operator()(const Dune::FieldMatrix<K,n,m>& x) const
    {
      Dune::FieldMatrix<K,m,n> y;
      for(std::size_t i=0; i<x.N(); ++i)
        for(std::size_t j=0; j<x.M(); ++j)
          y[j][i] = x[i][j];
      return y;
    }
  };



  struct SymOp {
    template<class Matrix>
    auto operator()(const Matrix& M) const
    {
      return 0.5*(M + M.transposed());
    }
  };



  struct TraceOp {
    template<class Matrix>
    auto operator()(const Matrix& M) const
    {
      using T = std::decay_t<decltype(M[0][0])>;
      T tr = 0;
      for (auto k : Dune::range(M.N()))
        tr += M[k][k];
      return tr;
    }
  };



  template<class Op>
  class TransposedBinaryOp
  {
  public:
    TransposedBinaryOp(const Op& op) : op_(op) {}

    template<class X, class Y>
    decltype(auto) operator()(X&& x, Y&& y) const
    {
      return op_(std::forward<Y>(y), std::forward<X>(x));
    }

  private:
    Op op_;
  };



} // namespace Dune::Fufem::Forms::LocalOperators



#endif // DUNE_FUFEM_FORMS_LOCALOPERATORS_HH
