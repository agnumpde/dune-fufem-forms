// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_INTEGRATEDBOUNDARYBILINEARFORM_HH
#define DUNE_FUFEM_FORMS_INTEGRATEDBOUNDARYBILINEARFORM_HH

#include <cstddef>
#include <type_traits>
#include <utility>

#include <dune/common/rangeutilities.hh>

#include <dune/typetree/childextraction.hh>

#include <dune/fufem/forms/shapefunctioncache.hh>
#include <dune/fufem/forms/baseclass.hh>
#include <dune/fufem/forms/localsumassembler.hh>



namespace Dune::Fufem::Forms {



  template<class BilinearOperator, class Patch, bool isSemiAffine=true>
  class IntegratedBoundaryBilinearForm
  {
    using TestRootBasis = std::tuple_element_t<0, typename BilinearOperator::Basis>;
    using TestRootLocalView = typename TestRootBasis::LocalView;
    using TestRootTree = typename TestRootLocalView::Tree;

    using AnsatzRootBasis = std::tuple_element_t<1, typename BilinearOperator::Basis>;
    using AnsatzRootLocalView = typename AnsatzRootBasis::LocalView;
    using AnsatzRootTree = typename AnsatzRootLocalView::Tree;

    using LocalOperator = decltype(localOperator(std::declval<BilinearOperator>()));

  public:
    using Element = typename BilinearOperator::Element;
    using Intersection = typename Patch::GridView::Intersection;
    using IntersectionIterator = typename Patch::GridView::IntersectionIterator;

    IntegratedBoundaryBilinearForm(const BilinearOperator& sumOperator, const Patch& patch) :
      sumOperator_(sumOperator),
      sumLocalOperator_(localOperator(sumOperator)),
      patch_(patch)
    {}

    /**
     * \brief Register local views
     *
     * This has to be called once, before using the assembler.
     * The passed local views must be the same that are used
     * when calling the assembler for on an element afterwards.
     */
    template<class TestLocalView, class AnsatzLocalView>
    void preprocess(const TestLocalView& testLocalView, const AnsatzLocalView& ansatzLocalView)
    {
      cache_.setTree(testLocalView.rootLocalView().tree());
      sumLocalOperator_.bindToLocalView(testLocalView.rootLocalView(), ansatzLocalView.rootLocalView());
    }

    template<class LocalMatrix, class TestLocalView, class AnsatzLocalView>
    void operator()(const Element& element, LocalMatrix& localMatrix, const TestLocalView& testSubspaceLocalView, const AnsatzLocalView& ansatzSubspaceLocalView)
    {
      if (not patch_.containsFaceOf(element))
        return;
      for(const auto& intersection : intersections(patch_.gridView(), element))
        if (patch_.contains(intersection))
          this->operator()(intersection, localMatrix, testSubspaceLocalView, ansatzSubspaceLocalView);
    }

    template<class LocalMatrix, class TestLocalView, class AnsatzLocalView>
    void operator()(const Intersection& intersection, LocalMatrix& localMatrix, const TestLocalView& testSubspaceLocalView, const AnsatzLocalView& ansatzSubspaceLocalView)
    {
      auto facet = intersection.indexInInside();
      const auto& testLocalView = testSubspaceLocalView.rootLocalView();
      const auto& ansatzLocalView = ansatzSubspaceLocalView.rootLocalView();

      const auto& geometry = intersection.geometry();

      sumLocalOperator_.bind(testLocalView.element());

      Impl::forEachTupleEntry(sumLocalOperator_.operators(), [&](auto& op) {
        auto& cacheForRule = cache_[std::pair(op.quadratureRuleKey(), facet)];
        cacheForRule.invalidate(isSemiAffine);
      });

      Impl::forEachTupleEntry(sumLocalOperator_.operators(), [&](auto& op) {
        auto& cacheForRule = cache_[std::pair(op.quadratureRuleKey(), facet)];

        const auto& testPrefix = std::get<0>(op.treePath());
        const auto& testNode = Dune::TypeTree::child(testLocalView.tree(), testPrefix);
        auto& testCache = cacheForRule[testPrefix];

        const auto& ansatzPrefix = std::get<1>(op.treePath());
        const auto& ansatzNode = Dune::TypeTree::child(ansatzLocalView.tree(), ansatzPrefix);
        auto& ansatzCache = cacheForRule[ansatzPrefix];

        op.bindToCache(testCache, ansatzCache);

        const auto& quad = cacheForRule.rule();

        for (auto k : Dune::range(quad.size()))
        {
          const auto& quadPoint = quad[k];
          auto quadPointPositionInFacet = intersection.geometryInInside().local(quadPoint.position());
          const auto integrationWeight = quadPoint.weight() * geometry.integrationElement(quadPointPositionInFacet);
          auto evaluatedOperator = op(IndexedReference(quadPoint.position(), k));
          for (auto i : Dune::range(testNode.size()))
            for (auto j : Dune::range(ansatzNode.size()))
              localMatrix[testNode.localIndex(i)][ansatzNode.localIndex(j)] += evaluatedOperator(i,j) * integrationWeight;
        }
      });
    }

    const BilinearOperator& integrandOperator() const
    {
      return sumOperator_;
    }

  private:
    const BilinearOperator sumOperator_;
    mutable LocalOperator sumLocalOperator_;
    FacetShapeFunctionMultiCache<TestRootTree> cache_;
    const Patch& patch_;
  };



  template<class BilinearOperator, class Patch, bool isSemiAffine>
  struct IsLocalAssembler<IntegratedBoundaryBilinearForm<BilinearOperator, Patch, isSemiAffine>> : public std::true_type {};



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_INTEGRATEDBOUNDARYBILINEARFORM_HH
