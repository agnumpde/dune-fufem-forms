// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_INTEGRATEDBOUNDARYLINEARFORM_HH
#define DUNE_FUFEM_FORMS_INTEGRATEDBOUNDARYLINEARFORM_HH

#include <cstddef>
#include <type_traits>
#include <utility>

#include <dune/common/rangeutilities.hh>

#include <dune/typetree/childextraction.hh>

#include <dune/fufem/boundarypatch.hh>

#include <dune/fufem/forms/shapefunctioncache.hh>
#include <dune/fufem/forms/baseclass.hh>
#include <dune/fufem/forms/localsumassembler.hh>



namespace Dune::Fufem::Forms {



  template<class LinearOperator, class Patch, bool isSemiAffine=true>
  class IntegratedBoundaryLinearForm
  {
    using TestRootBasis = typename LinearOperator::Basis;
    using TestRootLocalView = typename TestRootBasis::LocalView;
    using TestRootTree = typename TestRootLocalView::Tree;

    using LocalOperator = decltype(localOperator(std::declval<LinearOperator>()));

  public:
    using Element = typename LinearOperator::Element;
    using Intersection = typename Patch::GridView::Intersection;
    using IntersectionIterator = typename Patch::GridView::IntersectionIterator;

    IntegratedBoundaryLinearForm(const LinearOperator& sumOperator, const Patch& patch) :
      sumOperator_(sumOperator),
      sumLocalOperator_(localOperator(sumOperator)),
      patch_(patch)
    {}

    /**
     * \brief Register local view
     *
     * This has to be called once, before using the assembler.
     * The passed local view must be the same that is used
     * when calling the assembler for on an element afterwards.
     */
    template<class TestLocalView>
    void preprocess(const TestLocalView& testLocalView)
    {
      cache_.setTree(testLocalView.rootLocalView().tree());
      sumLocalOperator_.bindToLocalView(testLocalView.rootLocalView());
    }

    template<class LocalVector, class TestLocalView>
    void operator()(const Element& element, LocalVector& localVector, const TestLocalView& testSubspaceLocalView)
    {
      if (not patch_.containsFaceOf(element))
        return;
      for(const auto& intersection : intersections(patch_.gridView(), element))
        if (patch_.contains(intersection))
          this->operator()(intersection, localVector, testSubspaceLocalView);
    }

    template<class LocalVector, class TestLocalView>
    void operator()(const Intersection& intersection, LocalVector& localVector, const TestLocalView& testSubspaceLocalView)
    {
      auto facet = intersection.indexInInside();
      const auto& testLocalView = testSubspaceLocalView.rootLocalView();

      const auto& geometry = intersection.geometry();

      sumLocalOperator_.bind(testLocalView.element());

      Impl::forEachTupleEntry(sumLocalOperator_.operators(), [&](auto& op) {
        auto& cacheForRule = cache_[std::pair(op.quadratureRuleKey(), facet)];
        cacheForRule.invalidate(isSemiAffine);
      });

      Impl::forEachTupleEntry(sumLocalOperator_.operators(), [&](auto& op) {
        auto& cacheForRule = cache_[std::pair(op.quadratureRuleKey(), facet)];

        const auto& testPrefix = op.treePath();
        const auto& testNode = Dune::TypeTree::child(testLocalView.tree(), testPrefix);
        auto& testCache = cacheForRule[testPrefix];

        op.bindToCache(testCache);

        const auto& quad = cacheForRule.rule();

        for (auto k : Dune::range(quad.size()))
        {
          const auto& quadPoint = quad[k];
          auto quadPointPositionInFacet = intersection.geometryInInside().local(quadPoint.position());
          const auto integrationWeight = quadPoint.weight() * geometry.integrationElement(quadPointPositionInFacet);
          auto evaluatedOperator = op(IndexedReference(quadPoint.position(), k));
          for (auto i : Dune::range(testNode.size()))
            localVector[testNode.localIndex(i)] += evaluatedOperator(i) * integrationWeight;
        }
      });
    }

    const LinearOperator& integrandOperator() const
    {
      return sumOperator_;
    }

  private:
    const LinearOperator sumOperator_;
    LocalOperator sumLocalOperator_;
    FacetShapeFunctionMultiCache<TestRootTree> cache_;
    const Patch& patch_;
  };



  template<class LinearOperator, class Patch, bool isSemiAffine>
  struct IsLocalAssembler<IntegratedBoundaryLinearForm<LinearOperator, Patch, isSemiAffine>> : public std::true_type {};



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_INTEGRATEDBOUNDARYLINEARFORM_HH
