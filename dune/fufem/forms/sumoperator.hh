// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_SUMOPERATOR_HH
#define DUNE_FUFEM_FORMS_SUMOPERATOR_HH

#include <cstddef>
#include <type_traits>
#include <tuple>
#include <utility>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

#include <dune/fufem/forms/baseclass.hh>



namespace Dune::Fufem::Forms {



  // Sum of multilinear operators of the same arity k gives another multilinear
  // of arity k.
  template<class Operator0, class... Operators>
  class SumOperator
  {
    template <class T, class... Ts>
    struct AreInteroperable : std::conjunction<Dune::IsInteroperable<T, Ts>...> {};

  public:
    static constexpr std::size_t arity = Operator0::arity;

    using Basis = typename Operator0::Basis;
    using Element = typename Operator0::Element;
    using Domain = typename Operator0::Domain;
    using LocalDomain = typename Operator0::LocalDomain;
    using Range = typename Operator0::Range;

    SumOperator(Operator0 operator0, Operators... operators) :
      operators_(operator0, operators...)
    {
      static_assert(AreInteroperable<typename Operator0::Range, typename Operators::Range...>::value, "Trying do build sum of operators with non-interoperable ranges.");
    }

    class LocalOperator
    {
    public:

      LocalOperator(const typename Operator0::LocalOperator& localOperator0, const typename Operators::LocalOperator&... localOperators) :
        localOperators_(localOperator0, localOperators...)
      {}

      auto quadratureRuleKey() const
      {
        auto  quadKey = QuadratureRuleKey();
        Impl::forEachTupleEntry(localOperators_, [&](const auto& localOperator) {
          quadKey = localOperator.quadratureRuleKey().sum(quadKey);
        });
        return quadKey;
      }

      void bind(const Element& element)
      {
        Impl::forEachTupleEntry(localOperators_, [&](auto& localOperator) {
          localOperator.bind(element);
        });
      }

      void unbind()
      {
        Impl::forEachTupleEntry(localOperators_, [&](auto& localOperator) {
          localOperator.unbind();
        });
      }

      auto operator()(const LocalDomain& x) const
      {
        Range y;
        y = 0;
        Impl::forEachTupleEntry(localOperators_, [&](auto& localOperator) {
           y += localOperator(x);
        });
        return y;
      }

      template<class... LocalViews>
      void bindToLocalView(const LocalViews&... localViews)
      {
        Impl::forEachTupleEntry(localOperators_, [&](auto& localOperator) {
          localOperator.bindToLocalView(localViews...);
        });
      }

      auto& operators()
      {
        return localOperators_;
      }

      const auto& operators() const
      {
        return localOperators_;
      }

    private:
      std::tuple<typename Operator0::LocalOperator, typename Operators::LocalOperator...> localOperators_;
    };

    friend auto localOperator(const SumOperator& sumOperator)
    {
      return std::apply([&](const auto&... op) {
        return LocalOperator(localOperator(op)...);
      }, sumOperator.operators());
    }

    friend auto localFunction(const SumOperator& sumOperator)
    {
      return localOperator(sumOperator);
    }

    auto& operators()
    {
      return operators_;
    }

    const auto& operators() const
    {
      return operators_;
    }

    const auto& basis() const
    {
      return std::get<0>(operators()).basis();
    }

  private:
    std::tuple<Operator0, Operators...> operators_;
  };



//  template<class... Operators>
//  SumOperator(Operators...) -> SumOperator<Operators...>;

//  template<class... Operators>
//  SumOperator(SumOperator<Operators...>) -> SumOperator<Operators...>;



  template<class L, class R,
    std::enable_if_t<isOperator_v<L> and isOperator_v<R>, int> = 0>
  auto sum(const L& l, const R& r)
  {
    return SumOperator<L, R>(l, r);
  }

  template<class L, class... Ri,
    std::enable_if_t<isOperator_v<L>, int> = 0>
  auto sum(const L& l, const SumOperator<Ri...>& r)
  {
    return std::apply([&](const auto&... ri) {
        return SumOperator<L, Ri...>(l, ri...);
    }, r.operators());
  }

  template<class... Li, class R,
    std::enable_if_t<isOperator_v<R>, int> = 0>
  auto sum(const SumOperator<Li...>& l, const R& r)
  {
    return std::apply([&](const auto&... li) {
        return SumOperator<Li..., R>(li..., r);
    }, l.operators());
  }

  template<class... Li, class... Rj>
  auto sum(const SumOperator<Li...>& l, const SumOperator<Rj...>& r)
  {
    return std::apply([&](const auto&...li) {
      return std::apply([&](const auto&...rj) {
          return SumOperator<Li..., Rj...>(li..., rj...);
      }, r.operators());
    }, l.operators());
  }



  template<class L, class R,
    std::enable_if_t<isOperatorOrSumOperator_v<L> and isOperatorOrSumOperator_v<R>, int> = 0>
  auto operator+ (const L& l, const R& r)
  {
    static_assert(L::arity==R::arity, "The operators passed to operator+ do not have the same arity");
    return sum(l, r);
  }

  template<class L, class R,
    std::enable_if_t<isOperatorOrSumOperator_v<L> and isOperatorOrSumOperator_v<R>, int> = 0>
  auto operator- (const L& l, const R& r)
  {
    static_assert(L::arity==R::arity, "The operators passed to operator- do not have the same arity");
    return sum(l, -r);
  }



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_SUMOPERATOR_HH
