// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_INTEGRATE_HH
#define DUNE_FUFEM_FORMS_INTEGRATE_HH

#include <type_traits>
#include <utility>

#include <dune/fufem/boundarypatch.hh>

#include <dune/fufem/forms/baseclass.hh>

#include <dune/fufem/forms/integratedlinearform.hh>
#include <dune/fufem/forms/integratedbilinearform.hh>
#include <dune/fufem/forms/integratedboundarylinearform.hh>
#include <dune/fufem/forms/integratedboundarybilinearform.hh>



namespace Dune::Fufem::Forms {



  /**
   * \brief Tag type to classify non-affine finite element families
   *
   * Pass this flag to integrate(...) if the finite element family is
   * not affine in the following sense: A family is called affine, if
   * the local finite elements behaves identical on two elements
   * whenever the geometry type and the order coincide. This property
   * allows to cache evaluation of shape functions on quadrature rules
   * across elements.
   *
   * Using passing this flag any caching of shape function evaluations
   * across different elements is disabled.
   */
  class NonAffineFamiliyTag {};



  template<class MultilinearOperator,
    std::enable_if_t<isOperatorOrSumOperator_v<MultilinearOperator>, int> = 0>
  auto integrate(MultilinearOperator op)
  {
    auto sumOperator = SumOperator(op);
    if constexpr(MultilinearOperator::arity==1)
      return IntegratedLinearForm(sumOperator);
    else if constexpr(MultilinearOperator::arity==2)
      return IntegratedBilinearForm(sumOperator);
  }

  template<class MultilinearOperator, class GridView,
    std::enable_if_t<isOperatorOrSumOperator_v<MultilinearOperator>, int> = 0>
  auto integrate(MultilinearOperator op, const BoundaryPatch<GridView>& patch)
  {
    auto sumOperator = SumOperator(op);
    if constexpr(MultilinearOperator::arity==1)
      return IntegratedBoundaryLinearForm(sumOperator, patch);
    else if constexpr(MultilinearOperator::arity==2)
      return IntegratedBoundaryBilinearForm(sumOperator, patch);
  }



  template<class MultilinearOperator,
    std::enable_if_t<isOperatorOrSumOperator_v<MultilinearOperator>, int> = 0>
  auto integrate(MultilinearOperator op, NonAffineFamiliyTag)
  {
    auto sumOperator = SumOperator(op);
    using SumOperator = decltype(sumOperator);
    if constexpr(MultilinearOperator::arity==1)
      return IntegratedLinearForm<SumOperator, false>(sumOperator);
    else if constexpr(MultilinearOperator::arity==2)
      return IntegratedBilinearForm<SumOperator, false>(sumOperator);
  }

  template<class MultilinearOperator, class GridView,
    std::enable_if_t<isOperatorOrSumOperator_v<MultilinearOperator>, int> = 0>
  auto integrate(MultilinearOperator op, const BoundaryPatch<GridView>& patch, NonAffineFamiliyTag)
  {
    auto sumOperator = SumOperator(op);
    using SumOperator = decltype(sumOperator);
    using Patch = BoundaryPatch<GridView>;
    if constexpr(MultilinearOperator::arity==1)
      return IntegratedBoundaryLinearForm<SumOperator, Patch, false>(sumOperator, patch);
    else if constexpr(MultilinearOperator::arity==2)
      return IntegratedBoundaryBilinearForm<SumOperator, Patch, false>(sumOperator, patch);
  }



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_INTEGRATE_HH
