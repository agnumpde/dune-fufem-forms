// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_SHAPEFUNCTIONCACHE_HH
#define DUNE_FUFEM_FORMS_SHAPEFUNCTIONCACHE_HH

#include <type_traits>
#include <utility>
#include <list>

#include <dune/common/indices.hh>
#include <dune/common/tuplevector.hh>
#include <dune/common/version.hh>
#if !DUNE_VERSION_GTE(DUNE_GEOMETRY, 2, 9)
#include <dune/common/transpose.hh>
#endif

#include <dune/typetree/treepath.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/quadraturerules/subentityquadraturerule.hh>



namespace Dune::Fufem::Forms::Impl {

  template<class C, class... T>
  static constexpr decltype(auto) accessByTreePath(C&& container, const Dune::TypeTree::HybridTreePath<T...>& path)
  {
    if constexpr (sizeof...(T)==0)
      return container;
    else
#if DUNE_VERSION_GT(DUNE_TYPETREE, 2, 9)
      return accessByTreePath(container[path.front()], pop_front(path));
#else
    {
      auto head = path[Dune::Indices::_0];
      auto tailPath = Dune::unpackIntegerSequence([&](auto... i){
                    return Dune::TypeTree::treePath(path[Dune::index_constant<i+1>{}]...);
                  }, std::make_index_sequence<sizeof...(T)-1>());
      return accessByTreePath(container[head], tailPath);
    }
#endif
  }


} // end namespace Dune::Fufem::Forms::Impl



namespace Dune::Fufem::Forms {



  /**
   * \brief A hierarchic cache for storing shape function evaluations for a tree
   *
   * This caches evaluations for a whole local ansatz tree in the sense
   * of dune-functions and a single quadrature rule.
   * The cache is implemented as nested container. Given quadrature rule
   * and tree, a ShapeFunctionCache can be created using either
      \code
      auto cache = ShapeFunctionCache<Tree>(rule, tree);
      \endcode
   * or
      \code
      auto cache = ShapeFunctionCache<Tree>(rule);
      cache.setTree(tree);
      \endcode
   * Now, using a treePath to some node in the tree, the index k of
   * a quadrature point and the index j of a shape function, the cached value
   * can be obtained using
      \code
      const auto& values = cache[treePath].getValues()[k][j];
      const auto& jacobian = cache[treePath].getJacobians()[k][j];
      const auto& globalJacobian = cache[treePath].getGlobalJacobians()[k][j];
      \endcode
   * The hierarchic cache forms a tree, where each node allows access
   * to direct children using cache[index] or to descendents using
   * cache[treePath].
   * Cached values are only implemented for leaf nodes so far.
   * Notice that the associated ansatz tree and quadrature rule
   * are stored by reference. While construction of the ShapeFunctionCache
   * works with an unbound tree, the tree has to be bound, when requesting
   * cached values.
   */
  template<class Node, class CT=double, class Dummy=void>
  class ShapeFunctionCache;



  template<class Node, class CT>
  class ShapeFunctionCache<Node, CT, std::enable_if_t<Node::isLeaf>>
  {
  public:

    using QuadratureRule = Dune::QuadratureRule<CT, Node::Element::dimension>;

    // \Todo Use proper FEGlobalJacobian for surface meshes
    using FEValue = typename Node::FiniteElement::Traits::LocalBasisType::Traits::RangeType;
    using FEJacobian = typename Node::FiniteElement::Traits::LocalBasisType::Traits::JacobianType;
    using FEGlobalJacobian = typename Node::FiniteElement::Traits::LocalBasisType::Traits::JacobianType;

    using ValueCache = typename std::vector<std::vector<FEValue>>;
    using JacobianCache = typename std::vector<std::vector<FEJacobian>>;
    using GlobalJacobianCache = typename std::vector<std::vector<FEGlobalJacobian>>;

    ShapeFunctionCache(const QuadratureRule& rule, const Node& node)
    {
      setRule(rule);
      setTree(node);
    }

    ShapeFunctionCache(const QuadratureRule& rule) :
      node_(nullptr)
    {
      setRule(rule);
    }

    ShapeFunctionCache() = default;

    void setRule(const QuadratureRule& rule)
    {
      rule_ = &rule;
      valueCache_.resize(rule_->size());
      jacobianCache_.resize(rule_->size());
      globalJacobianCache_.resize(rule_->size());
    }

    void setTree(const Node& node)
    {
      node_ = &node;
    }

    const QuadratureRule& rule() const
    {
      return *rule_;
    }

    void invalidate(bool isSemiAffine)
    {
      // We use the size at the first quadrature point to indicate
      // an invalidated cache. To this end we set it's size to zero.
      if (not isSemiAffine)
      {
        valueCache_[0].clear();
        jacobianCache_[0].clear();
      }
      globalJacobianCache_[0].clear();
    }

    decltype(auto) operator[](const Dune::TypeTree::HybridTreePath<>& treePath) const
    {
      return *this;
    }

    decltype(auto) operator[](const Dune::TypeTree::HybridTreePath<>& treePath)
    {
      return *this;
    }

    const auto& getValues()
    {
      if (valueCache_[0].empty())
      {
        const auto& localBasis = node_->finiteElement().localBasis();
        for(std::size_t k=0; k<rule_->size(); ++k)
          localBasis.evaluateFunction((*rule_)[k].position(), valueCache_[k]);
      }
      return valueCache_;
    }

    const auto& getJacobians()
    {
      if (jacobianCache_[0].empty())
      {
        const auto& localBasis = node_->finiteElement().localBasis();
        for(std::size_t k=0; k<rule_->size(); ++k)
          localBasis.evaluateJacobian((*rule_)[k].position(), jacobianCache_[k]);
      }
      return jacobianCache_;
    }

    const auto& getGlobalJacobians()
    {
      if (globalJacobianCache_[0].empty())
      {
        const auto& geometry = node_->element().geometry();
        const auto& jacobians = getJacobians();
        for(std::size_t k=0; k<rule_->size(); ++k)
        {
          globalJacobianCache_[k].resize(jacobians[k].size());

#if DUNE_VERSION_GTE(DUNE_GEOMETRY, 2, 9)
          const auto& jacobianInverse = geometry.jacobianInverse((*rule_)[k].position());
#else
          const auto& jacobianInverse = transpose(geometry.jacobianInverseTransposed((*rule_)[k].position()));
#endif
          for(std::size_t i=0; i<jacobians[k].size(); ++i)
            globalJacobianCache_[k][i] = jacobians[k][i] * jacobianInverse;
        }
      }
      return globalJacobianCache_;
    }

  private:
    const QuadratureRule* rule_ = nullptr;
    const Node* node_ = nullptr;
    ValueCache valueCache_;
    JacobianCache jacobianCache_;
    GlobalJacobianCache globalJacobianCache_;
  };



  template<class Node, class CT>
  class ShapeFunctionCache<Node, CT, std::enable_if_t<Node::isPower>>
  {
    using ChildNode = typename Node::ChildType;
    using ChildCache = ShapeFunctionCache<ChildNode, CT>;
  public:

    using QuadratureRule = Dune::QuadratureRule<CT, Node::Element::dimension>;

    ShapeFunctionCache(const QuadratureRule& rule, const Node& node) :
      childCache_(rule, node.child(0))
    {}

    ShapeFunctionCache(const QuadratureRule& rule) :
      childCache_(rule)
    {}

    ShapeFunctionCache() = default;

    void setRule(const QuadratureRule& rule)
    {
      childCache_.setRule(rule);
    }

    void setTree(const Node& node)
    {
      childCache_.setTree(node.child(0));
    }

    const QuadratureRule& rule() const
    {
      return childCache_.rule();
    }

    void invalidate(bool isSemiAffine)
    {
      childCache_.invalidate(isSemiAffine);
    }

    const ChildCache& operator[](std::size_t i) const
    {
      assert(i<Node::degree());
      return childCache_;
    }

    ChildCache& operator[](std::size_t i)
    {
      assert(i<Node::degree());
      return childCache_;
    }

    template<class... T>
    decltype(auto) operator[](const Dune::TypeTree::HybridTreePath<T...>& treePath) const
    {
      return Impl::accessByTreePath(*this, treePath);
    }

    template<class... T>
    decltype(auto) operator[](const Dune::TypeTree::HybridTreePath<T...>& treePath)
    {
      return Impl::accessByTreePath(*this, treePath);
    }

    constexpr std::size_t size() const noexcept
    {
      return Node::degree();
    }

  private:
    ChildCache childCache_;
  };



  namespace Impl {

    template<class List, class CT>
    struct TupleVectorOfShapeFunctionCaches
    {};

    template<class CT, template<class...> class ListType, class... Args>
    struct TupleVectorOfShapeFunctionCaches<ListType<Args...>, CT>
    {
      using type = Dune::TupleVector<ShapeFunctionCache<Args, CT>...>;
    };

    template<class List, class CT>
    using TupleVectorOfShapeFunctionCaches_t = typename TupleVectorOfShapeFunctionCaches<List, CT>::type;

  } // end namespace Imp



  template<class Node, class CT>
  class ShapeFunctionCache<Node, CT, std::enable_if_t<Node::isComposite>>
    : public Impl::TupleVectorOfShapeFunctionCaches_t<typename Node::ChildTypes, CT>
  {
    using Base = Impl::TupleVectorOfShapeFunctionCaches_t<typename Node::ChildTypes, CT>;
  public:

    using QuadratureRule = Dune::QuadratureRule<CT, Node::Element::dimension>;

    ShapeFunctionCache(const QuadratureRule& rule, const Node& node)
    {
      setRule(rule);
      setTree(node);
    }

    ShapeFunctionCache(const QuadratureRule& rule)
    {
      setRule(rule);
    }

    ShapeFunctionCache() = default;

    void setRule(const QuadratureRule& rule)
    {
      Hybrid::forEach(*this, [&](auto& childCache) {
        childCache.setRule(rule);
      });
    }

    void setTree(const Node& node)
    {
      Hybrid::forEach(Dune::range(Node::degree()), [&](const auto& i){
        (*this)[i].setTree(node.child(i));
      });
    }

    const QuadratureRule& rule() const
    {
      return (*this)[Dune::Indices::_0].rule();
    }

    using Base::operator[];

    template<class... T>
    decltype(auto) operator[](const Dune::TypeTree::HybridTreePath<T...>& treePath) const
    {
      return Impl::accessByTreePath(*this, treePath);
    }

    template<class... T>
    decltype(auto) operator[](const Dune::TypeTree::HybridTreePath<T...>& treePath)
    {
      return Impl::accessByTreePath(*this, treePath);
    }

    void invalidate(bool isSemiAffine) {
      Hybrid::forEach(*this, [&](auto& childCache) {
        childCache.invalidate(isSemiAffine);
      });
    }

  };



  template<class Tree, class CT=double>
  class ShapeFunctionMultiCache :
    public std::map<QuadratureRuleKey, ShapeFunctionCache<Tree, CT>>
  {
    using Key = QuadratureRuleKey;
    using Value = ShapeFunctionCache<Tree,CT>;
    using Base = std::map<Key, Value>;

    Value createCache(const Key& key) const
    {
      if (tree_!=nullptr)
        return ShapeFunctionCache<Tree,CT>(QuadratureRuleCache<CT, Tree::Element::dimension>::rule(key), *tree_);
      else
        return ShapeFunctionCache<Tree,CT>(QuadratureRuleCache<CT, Tree::Element::dimension>::rule(key));
    }

  public:

    ShapeFunctionMultiCache() = default;

    Value& operator[](const Key& key) {
      auto it = this->find(key);
      if (it == this->end())
        return (this->insert(std::make_pair(key, createCache(key)))).first->second;
      else
        return it->second;
    }

    void setTree(const Tree& tree) {
      tree_ = &tree;
      for(auto& [key,value] : *this)
        value.setTree(tree);
    }

  private:
    const Tree* tree_ = nullptr;
  };



  template<class Tree, class CT=double>
  class FacetShapeFunctionMultiCache :
    public std::map<std::pair<QuadratureRuleKey, std::size_t>, ShapeFunctionCache<Tree, CT>>
  {
    using Key = std::pair<QuadratureRuleKey, std::size_t>;
    using Value = ShapeFunctionCache<Tree,CT>;
    using Base = std::map<Key, Value>;
    using FaceQuadratureRule = Dune::Fufem::SubEntityQuadratureRule<CT, Tree::Element::dimension, 1>;

    Value createCache(const Key& key)
    {
      static constexpr auto dim = Tree::Element::dimension;
      auto facet = key.second;
      auto&& type = key.first.geometryType();
      auto facetGeometryType = Dune::referenceElement<CT,dim>(type).type(facet, 1);
      auto facetQuadratureRuleKey = key.first;
      facetQuadratureRuleKey.setGeometryType(facetGeometryType);
      const auto& rule = QuadratureRuleCache<CT, dim-1>::rule(facetQuadratureRuleKey);
      // We need to store the face quadrature rule.
      // By using an std::list, we can store pointers
      // to the stored rules, because they never get invalidated.
      faceQuadratureRules_.push_back(FaceQuadratureRule(type, facet, rule));
      if (tree_!=nullptr)
        return ShapeFunctionCache<Tree,CT>(faceQuadratureRules_.back(), *tree_);
      else
        return ShapeFunctionCache<Tree,CT>(faceQuadratureRules_.back());
    }

  public:

    FacetShapeFunctionMultiCache() = default;

    Value& operator[](const Key& key) {
      auto it = this->find(key);
      if (it == this->end())
        return (this->insert(std::make_pair(key, createCache(key)))).first->second;
      else
        return it->second;
    }

    void setTree(const Tree& tree) {
      tree_ = &tree;
      for(auto& [key,value] : *this)
        value.setTree(tree);
    }

  private:
    const Tree* tree_ = nullptr;
    std::list<FaceQuadratureRule> faceQuadratureRules_;
  };



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_SHAPEFUNCTIONCACHE_HH

