// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_PRODUCTOPERATOR_HH
#define DUNE_FUFEM_FORMS_PRODUCTOPERATOR_HH

#include <cstddef>
#include <type_traits>
#include <tuple>
#include <utility>

#include <dune/fufem/forms/baseclass.hh>
#include <dune/fufem/forms/localoperators.hh>
#include <dune/fufem/forms/transformedoperator.hh>
#include <dune/fufem/forms/sumoperator.hh>



namespace Dune::Fufem::Forms {


  template<class C, class Operator0, class Operator1>
  class ProductWithNullaryOperator : public Operator0
  {
    using Contraction = C;
    using LocalOperator0 = decltype(localOperator(std::declval<Operator0>()));
    using LocalOperator1 = decltype(localOperator(std::declval<Operator1>()));

  public:

    using Element = typename Operator0::Element;
    using Domain = typename Operator0::Domain;
    using LocalDomain = typename Operator0::LocalDomain;
    using Range = decltype(std::declval<Contraction>()(std::declval<typename Operator0::Range>(), std::declval<typename Operator1::Range>()));

    ProductWithNullaryOperator(const Contraction& product, const Operator0& operator0, const Operator1& operator1) :
      Operator0(operator0),
      operator1_(operator1),
      product_(product)
    {
      static_assert(isOperator_v<Operator0>, "The first factor of ProductWithNullaryOperator must be a operator.");
      static_assert(isOperator_v<Operator1>, "The second factor of ProductWithNullaryOperator must be a operator.");
      static_assert(Operator1::arity==0, "The second factor of ProductWithNullaryOperator needs to be nullary.");
    }

    class LocalOperator : public LocalOperator0
    {
    public:
      LocalOperator(const Contraction& product, LocalOperator0&& localOperator0, LocalOperator1&& localOperator1) :
        LocalOperator0(std::move(localOperator0)),
        localOperator1_(std::move(localOperator1)),
        product_(product)
      {}

      auto quadratureRuleKey() const
      {
        return quadratureRuleKey_;
      }

      void bind(const Element& element)
      {
        LocalOperator0::bind(element);
        localOperator1_.bind(element);
        quadratureRuleKey_ = LocalOperator0::quadratureRuleKey().product(localOperator1_.quadratureRuleKey());
      }

      void unbind()
      {
        LocalOperator0::unbind();
        localOperator1_.unbind();
      }

      template<class AugmentedLocalDomain>
      auto operator()(const AugmentedLocalDomain& x) const
      {
        auto result0 = LocalOperator0::operator()(x);
        auto result1 = localOperator1_(x);
        if constexpr (Operator0::arity == 0)
          return product_(result0, result1);
        else
          return [=](const auto&... indices) {
            return product_(result0(indices...), result1);
          };
      }

    private:
      LocalOperator1 localOperator1_;
      Contraction product_;
      QuadratureRuleKey quadratureRuleKey_;
    };

    friend LocalOperator localOperator(const ProductWithNullaryOperator& productOperator)
    {
      return LocalOperator(productOperator.contraction(), localOperator(productOperator.operator0()), localOperator(productOperator.operator1()));
    }

    friend LocalOperator localFunction(const ProductWithNullaryOperator& productOperator)
    {
      return localOperator(productOperator);
    }

    const Operator0& operator0() const
    {
      return static_cast<const Operator0&>(*this);
    }

    const Operator1& operator1() const
    {
      return operator1_;
    }

    const Contraction& contraction() const
    {
      return product_;
    }

  private:
    Operator1 operator1_;
    Contraction product_;
  };



  // Generic product of multilinear operators. This combines
  // a k_1-linear map F_1 with a k_2-linear map F_2
  // into one (k_1+k_2)-linear map using the pointwise
  // bilinear map P on the range spaces.
  template<class P, class Operator0, class Operator1>
  class UnaryUnaryProductOperator : public Dune::Fufem::Forms::MultilinearOperator<2>
  {
    using Contraction = P;
    using LocalOperator0 = decltype(localOperator(std::declval<Operator0>()));
    using LocalOperator1 = decltype(localOperator(std::declval<Operator1>()));

  public:
    using Basis = std::tuple<typename Operator0::Basis, typename Operator1::Basis>;

    using Element = typename Operator0::Element;
    using Domain = typename Operator0::Domain;
    using LocalDomain = typename Operator0::LocalDomain;
    using Range = decltype(std::declval<Contraction>()(std::declval<typename Operator0::Range>(), std::declval<typename Operator1::Range>()));

    UnaryUnaryProductOperator(const Contraction& product, const Operator0& operator0, const Operator1& operator1) :
      product_(product),
      operator0_(operator0),
      operator1_(operator1)
    {
      static_assert(isOperator_v<Operator0>, "The first factor of UnaryUnaryProductOperator must be a operator.");
      static_assert(isOperator_v<Operator1>, "The second factor of UnaryUnaryProductOperator must be a operator.");
      static_assert(Operator0::arity == 1);
      static_assert(Operator1::arity == 1);
      static_assert(Operator0::argIndex == 0);
      static_assert(Operator1::argIndex == 1);
    }

    class LocalOperator
    {
    public:
      LocalOperator(const Contraction& product, LocalOperator0&& localOperator0, LocalOperator1&& localOperator1):
        product_(product),
        localOperator0_(std::move(localOperator0)),
        localOperator1_(std::move(localOperator1))
      {}

      auto quadratureRuleKey() const
      {
        return quadratureRuleKey_;
      }

      void bind(const Element& element)
      {
        localOperator0_.bind(element);
        localOperator1_.bind(element);
        quadratureRuleKey_ = localOperator0_.quadratureRuleKey().product(localOperator1_.quadratureRuleKey());
      }

      void unbind()
      {
        localOperator0_.unbind();
        localOperator1_.unbind();
      }

      template<class LocalView0, class LocalView1>
      void bindToLocalView(const LocalView0& localView0, const LocalView1& localView1)
      {
        localOperator0_.bindToLocalView(localView0);
        localOperator1_.bindToLocalView(localView1);
      }

      template<class Cache0, class Cache1>
      void bindToCache(Cache0& cache0, Cache1& cache1)
      {
        localOperator0_.bindToCache(cache0);
        localOperator1_.bindToCache(cache1);
      }

      auto treePath() const
      {
        return std::tie(localOperator0_.treePath(), localOperator1_.treePath());
      }

      template<class AugmentedLocalDomain>
      auto operator()(const AugmentedLocalDomain& x) const
      {
        auto result0 = localOperator0_(x);
        auto result1 = localOperator1_(x);
        return [=](const auto& i, const auto& j) {
          return product_(result0(i), result1(j));
        };
      }

    private:
      Contraction product_;
      LocalOperator0 localOperator0_;
      LocalOperator1 localOperator1_;
      QuadratureRuleKey quadratureRuleKey_;
    };

    friend LocalOperator localOperator(const UnaryUnaryProductOperator& productOperator)
    {
      return LocalOperator(productOperator.contraction(), localOperator(productOperator.operator0()), localOperator(productOperator.operator1()));
    }

    friend LocalOperator localFunction(const UnaryUnaryProductOperator& productOperator)
    {
      return localOperator(productOperator);
    }

    const Operator0& operator0() const
    {
      return operator0_;
    }

    const Operator1& operator1() const
    {
      return operator1_;
    }

    const Contraction& contraction() const
    {
      return product_;
    }

    auto treePath() const
    {
      return std::tie(operator0_.treePath(), operator1_.treePath());
    }

  private:
    Contraction product_;
    Operator0 operator0_;
    Operator1 operator1_;
  };



  // The following are the generic functions for producing
  // product operators from a binary operator and two other
  // terms. At least one of those terms should be a operator.

  // Overload for two plain operators
  template<class Op, class L, class R,
    std::enable_if_t<isOperator_v<L> and isOperator_v<R>, int> = 0,
    std::enable_if_t<Dune::IsCallable<Op(typename L::Range, typename R::Range)>::value, int> = 0>
  auto product(const Op& op, const L& l, const R& r)
  {
    static_assert(L::arity+R::arity<=2, "Trying to construct multi-linear operator with rank>2");
    if constexpr(R::arity==0)
      return ProductWithNullaryOperator(op, l, r);
    else if constexpr(L::arity==0)
      return ProductWithNullaryOperator(LocalOperators::TransposedBinaryOp(op), r, l);
    else if constexpr((L::argIndex==0) and (R::argIndex==1))
      return UnaryUnaryProductOperator(op, l, r);
    else if constexpr((L::argIndex==1) and (R::argIndex==0))
      return UnaryUnaryProductOperator(LocalOperators::TransposedBinaryOp(op), r, l);
  }

  // Overload for one raw value and one operator
  // This will wrap the raw value into a operator.
  template<class Op, class L, class R,
    std::enable_if_t<(not isOperator_v<L>) and isOperator_v<R>, int> = 0,
    std::enable_if_t<Dune::IsCallable<Op(L, typename R::Range)>::value, int> = 0>
  auto product(const Op& op, const L& l, const R& r)
  {
    return compose([op, l](auto&& r_value) { return op(l, r_value); }, r);
  }

  // Overload for one operator and one raw value
  // This will wrap the raw value into a operator.
  template<class Op, class L, class R,
    std::enable_if_t<isOperator_v<L> and (not isOperator_v<R>), int> = 0,
    std::enable_if_t<Dune::IsCallable<Op(typename L::Range, R)>::value, int> = 0>
  auto product(const Op& op, const L& l, const R& r)
  {
    return compose([op, r](auto&& l_value) { return op(l_value, r); }, l);
  }

  // Overload for one sum-operator and one plain term
  // This will multiply out the product for the sum-operator.
  template<class Op, class... Li, class R,
    class = std::void_t<decltype((product(std::declval<Op>(), std::declval<Li>(), std::declval<R>()),...))>>
  auto product(const Op& op, const SumOperator<Li...>& l, const R& r)
  {
    return std::apply([&](const auto&... li) {
        return (product(op, li, r) + ...);
    }, l.operators());
  }

  // Overload for one plain term and one sum-operator
  // This will multiply out the product for the sum-operator.
  template<class Op, class L, class... Ri,
    class = std::void_t<decltype((product(std::declval<Op>(), std::declval<L>(), std::declval<Ri>()),...))>>
  auto product(const Op& op, const L& l, const SumOperator<Ri...>& r)
  {
    return std::apply([&](const auto&...ri) {
        return (product(op, l, ri) + ...);
    }, r.operators());
  }

  // Overload for two sum-operators
  // This will multiply out the product for both sum-operators.
  template<class Op, class... Li, class... Rj,
    class = std::void_t<decltype((product(std::declval<Op>(), std::declval<Li>(), std::declval<SumOperator<Rj...>>()),...))>>
  auto product(const Op& op, const SumOperator<Li...>& l, const SumOperator<Rj...>& r)
  {
    return std::apply([&](const auto&...li) {
      return (product(op, li, r) + ...);
    }, l.operators());
  }



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_PRODUCTOPERATOR_HH
