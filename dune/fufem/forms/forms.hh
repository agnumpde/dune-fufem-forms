// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_ASSEMBLERS_FORMS_HH
#define DUNE_FUFEM_ASSEMBLERS_FORMS_HH


#include <dune/fufem/forms/shapefunctioncache.hh>

#include <dune/fufem/forms/baseclass.hh>
#include <dune/fufem/forms/localoperators.hh>
#include <dune/fufem/forms/nullaryoperators.hh>
#include <dune/fufem/forms/transformedoperator.hh>
#include <dune/fufem/forms/unaryoperators.hh>
#include <dune/fufem/forms/sumoperator.hh>
#include <dune/fufem/forms/productoperator.hh>
#include <dune/fufem/forms/operators.hh>
#include <dune/fufem/forms/localsumassembler.hh>
#include <dune/fufem/forms/integratedlinearform.hh>
#include <dune/fufem/forms/integratedbilinearform.hh>
#include <dune/fufem/forms/integratedboundarylinearform.hh>
#include <dune/fufem/forms/integratedboundarybilinearform.hh>
#include <dune/fufem/forms/integrate.hh>
#include <dune/fufem/forms/boundunaryoperator.hh>



#endif // DUNE_FUFEM_ASSEMBLERS_FORMS_HH
