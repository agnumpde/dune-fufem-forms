// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_NULLARYOPERATORS_HH
#define DUNE_FUFEM_FORMS_NULLARYOPERATORS_HH

#include <type_traits>
#include <utility>

#include <dune/common/typetraits.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/matrixindexset.hh>

#include <dune/fufem/forms/baseclass.hh>



namespace Dune::Fufem::Forms {



  // Zero-linear map induced by fixed coefficient function
  template<class F>
  class Coefficient : public F, public MultilinearOperator<0>
  {
    using LocalFunction = std::decay_t<decltype(localFunction(std::declval<F&>()))>;

  public:

    using Element = typename F::EntitySet::Element;
    using Domain = typename Element::Geometry::GlobalCoordinate;
    using LocalDomain = typename Element::Geometry::LocalCoordinate;
    using Range = decltype(std::declval<LocalFunction>()(std::declval<LocalDomain>()));

    Coefficient(const F& f) :
      F(f),
      order_(0)
    {}

    Coefficient(const F& f, std::size_t order) :
      F(f),
      order_(order)
    {}

    class LocalOperator : public LocalFunction
    {
    public:
      LocalOperator(LocalFunction&& fLocal, std::size_t order) :
        LocalFunction(std::move(fLocal)),
        order_(order)
      {}

      auto quadratureRuleKey() const
      {
        return QuadratureRuleKey(Element::dimension, order_);
      }

    private:
      std::size_t order_;
    };

    friend LocalOperator localOperator(const Coefficient& coefficient)
    {
      return LocalOperator(localFunction(coefficient), coefficient.order_);
    }

  private:
    std::size_t order_;
  };



} // namespace Dune::Fufem::Forms



#endif // DUNE_FUFEM_FORMS_NULLARYOPERATORS_HH
