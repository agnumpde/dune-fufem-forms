% -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
% vi: set et ts=4 sw=2 sts=2:
\pdfoutput=1
\documentclass[a4paper,10pt,headings=normal,bibliography=totoc]{scrartcl}

\usepackage{scrhack}  % Fix a LaTeX warning

\usepackage{authblk}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[utf8]{inputenc}
\usepackage{fancyhdr}
\pagestyle{fancy}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{bm}
\usepackage{colonequals}
\usepackage{overpic}
\usepackage{url}
\usepackage{xspace}
\usepackage[square,numbers,sort]{natbib}

\usepackage[colorinlistoftodos]{todonotes}
%\usepackage[colorinlistoftodos,disable]{todonotes}
\usepackage{environ}
\usepackage{enumitem}
\usepackage{listings}
\usepackage{float}
\usepackage{minibox}


\usepackage{tikz}
\usetikzlibrary{arrows}
\tikzset{
    treenode/.style = {
        align=center,
        inner sep=0pt,
        text centered,
        font=\sffamily,
        rectangle,
        rounded corners=3mm,
        draw=black,
        minimum width=2em,
        minimum height=2em,
        inner sep=1mm,
        outer sep=0mm
    },
    smalltreenode/.style = {
        treenode,
        font=\footnotesize
    },
    basisnode/.style = {
        treenode,
        minimum width=9mm,
    },
    smallbasisnode/.style = {
        basisnode,
        font=\footnotesize
    }
}

% Seed for the random matrix occupation patterns
\pgfmathsetseed{\number\pdfrandomseed}
%\pgfmathsetseed{\number666}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%   hyperref should be loaded late to avoid incompatibilities
\usepackage{hyperref}

\hypersetup{pdftitle={The dune-fufem framework for defining local assemblers for linear and bilinear forms},
            pdfauthor={Carsten Gräser} }


\usepackage{attachfile2}
\usepackage{fancyvrb}
\usepackage{color}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%     Settings for the listings package

\definecolor{light-gray}{gray}{0.95}
\lstset{language={c++},
        basicstyle=\ttfamily\small,
%        commentstyle=\textit,
        commentstyle=\rmfamily\textit,
        columns=fixed,
%        columns=flexible,
        basewidth={0.5em,0.45em},
        escapeinside={/*@}{@*/},
        moredelim=**[is][\color{blue}]{@@}{@@},
        xleftmargin=1em,
        xrightmargin=1em,
        backgroundcolor=\color{light-gray},
        frame=single,
        }

% How to include ranges of an external source code file
\lstset{rangeprefix=//\ \{\ ,% curly left brace plus space, all in a C++-style comment
        rangesuffix=\ \},% space plus curly right brace
        numberstyle=\footnotesize,  % font size for numbers
        includerangemarker=false}  % Do not show the range markers

\definecolor{interfacecolor}{rgb}{0.95,0.95,1}
\lstdefinestyle{Example}{}
%\lstdefinestyle{Interface}{backgroundcolor=\color{interfacecolor},frame=single,frameround={tttt}}
\lstdefinestyle{Interface}{backgroundcolor=\color{interfacecolor},frame=single}

\newcommand{\cpp}[1]{\lstinline[basicstyle=\ttfamily]!#1!}
\newcommand{\cppbreak}[1]{\lstinline[basicstyle=\ttfamily,breaklines]!#1!}



\newtheorem{definition}{Definition}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\R}{\mathbb{R}}
\newcommand{\T}{\mathbb{T}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\V}{\mathbb{V}}

\newcommand{\Rcal}{\mathcal{R}}

\newcommand{\abs}[1]{{\lvert#1\rvert}}
\newcommand{\norm}[1]{\lVert#1\rVert}
\newcommand{\op}[1]{\operatorname{#1}}
\newcommand{\st}{\; : \;}
\renewcommand{\div}{\operatorname{div}}
\DeclareMathOperator{\trace}{tr}

\newcommand{\dune}{\textsc{Dune}\xspace}
\newcommand{\program}[1]{\textsc{#1}\xspace}



% For typesetting Dune module names
\newcommand{\dunemodule}[1]{\texttt{#1}}
% For typesetting file names
\newcommand{\file}[1]{\texttt{#1}}

\definecolor{lightblue}{HTML}{55AAFF}

\newcommand{\todograeser}[1]{\todo[inline,color=lightblue,author=CG]{#1}}

\newcommand{\IMPdiscuss}[1]{\todo[noline,color=lightblue]{\footnotesize \textbf{Discuss:}\\ #1}}
\newcommand{\IMPtodo}[1]{\todo[noline,color=lightblue]{\footnotesize \textbf{ToDo:}\\ #1}}

%%  All graphics files must be in this subdirectory
\graphicspath{{gfx/}}

% Silence the infamous ``multiple pdfs with page group included in a single page'' warning
\pdfsuppresswarningpagegroup=1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{The dune-fufem discretization module}
\author[1]{Carsten Gräser}
\renewcommand\Affilfont{\normalsize}
\affil[1]{FAU Erlangen-Nürnberg, Department Mathematik, graeser@math.fau.de}
%\date{}

\lhead{C.~Gräser}
\rhead{The dune-fufem-forms framework}

\begin{document}

\maketitle

\begin{center}
%\minibox[frame,c]{
%  This work is licensed under a\\
%  \emph{Creative Commons Attribution-NoDerivatives 4.0 International License}.\\
%  \includegraphics[width=3cm]{cc-by-nd.pdf}\\
%  The full license text is available here:\\
%  \url{https://creativecommons.org/licenses/by-nd/4.0/legalcode}
%  }
\end{center}

\begin{abstract}
  This document describes the
  \dunemodule{dune-fufem} \dune module
  for finite element discretizations
  based on the function and function space basis
  interfaces of the \dunemodule{dune-functions} module.
\end{abstract}



%\section{Introduction}



%\section{Boundary patches}



%\section{Utilities for thread parallel algorithms}



%\section{Global assemblers}



\section{Defining local assemblers by expressions trees}

This section describes a framework
for defining local assemblers for linear and bilinear forms
using nested expressions.
The interface classes and functions provided are all contained in the
namespace \cppbreak{Dune::Fufem::Forms}.
We will omit this namespace throughout this section.
In user code one either has to use qualified names,
import this namespace, or rely on ADL (argument dependent lookup)
where possible.
The main building blocks for defining linear and bilinear forms
are elementary unary and nullary operators.
These can be combined to more complex operators by local transformation,
multiplication, and addition.

The framework discussed in the following
essentially allows to define such nested operators
using a simple and convenient syntax.
Notice that, although the syntax was motivated by the
\emph{unified form language} (UFL)
introduced by the Fenics project \cite{AlnaesLoggEtAl2014},
we do not aim to implement UFL exactly, but rather
provide an interface that works well with
the interfaces for functions \cite{EngwerEtAl2017}
and function space bases \cite{EngwerEtAl2018}
of the \dunemodule{dune-functions} module.

Before we discuss these building blocks
we introduce some abstract concept.

\subsection{Abstract concepts}

In the following let $\Omega \subset \R^d$ be an open, nonempty domain.
We are interested in defining linear functional $\ell(\cdot)$ and bilinear
forms $a(\cdot,\cdot)$ of the form
\begin{align*}
  a(w,v) = \int P(\mathcal{L}_1(v)(x),\mathcal{L}_2(w)(x)) d\mu(x),
  \qquad
  \ell(v) = \int \mathcal{L}_3(v)(x) d\mu(x)
\end{align*}
where $\mathcal{L}_1,\mathcal{L}_2,\mathcal{L}_3$ are differential operators
$P(\cdot,\cdot)$ is a bilinear form on the according range spaces of
$\mathcal{L}_1(v)$ and $\mathcal{L}_2(v)$ and $\mu$ is a measure on $\overline{\Omega}$.

All considered functions live in function spaces of
scalar-, vector-, and matrix-valued functions on $\overline{\Omega}$.
To formalize this, we denote by $\T_r$ the set of
$\R$-tensor spaces of rank $r \in \N_0$ given by
\begin{align*}
  \T_r = \{ \R^{n_1 \times \dots \times n_r} \st n_1, \dots, n_k \in \N\} \quad \forall r \in \N,
  \qquad \T_0 = \{\R\}
\end{align*}
where use the convention that $\N= \{1,\dots \}$ and $\N_0 = \{0,1,\dots \}$.
The union of all such tensor spaces with any rank is denoted by
\begin{align*}
  \T = \bigcup_{r \in \N_0} \T_r.
\end{align*}
We will use these spaces as ranges of the functions to be considered.
Hence the most interesting cases are covered by $\T_0 \cup \T_1 \cup \T_2$
representing scalars, vectors, and matrices.

The main objective of our framework will be function spaces of
integrable tensor fields on $\Omega$, i.e., function spaces
\begin{align}
  \label{eq:tensor_field_space}
  V \subset L^1(\Omega; R) \qquad \text{ for some } R\in \T.
\end{align}
For convenience we introduce the notation
\begin{align*}
  \V &= \bigcup_{R \in \T} \V_R, &
  \V_R &= \{ V \subset L^1(\Omega; R) \st V \text{ is a subspace}\} \quad \text{ for fixed } R\in \T
\end{align*}
for the set of all such vector spaces, such that
$V \in \V$ simply means that
$V$ is a tensor field space in the sense of \eqref{eq:tensor_field_space}.
The range space of $V \in \V$ will be denoted by $R(V) \in \T$, i.e.,
$R: \V \to \T$.

Our final goal is to introduce linear and bilinear forms
on function spaces from $\V$ representing weak forms of
partial differential equations, that are typically obtained
by integration over $\Omega$, its boundary $\partial \Omega$,
or subsets. A common example is the weak form
\begin{align}
  \label{eq:poisson}
  \underbrace{\int_\Omega \alpha(x)\nabla u(x) \cdot \nabla v(x) \,dx}_{=:a(u,v)}
  = \underbrace{\int_\Omega f(x) v(x) \,dx}_{=:\ell(v)}
\end{align}
of a Poisson equation with diffusivity matrix $\alpha$.
Here, the left and right hand side are bilinear and linear forms over suitable
function spaces. The integrands on both sides are again functions
from some function space $W \subset L^1(\Omega,\R)$ that depend linearly
on $u$ and $v$.

Motivated by this observation we now consider multilinear operators
mapping function spaces to function spaces.
More precisely, we say that $T$ is a $K$-linear
function operator (with $K \in \N$),
if $T:V_1 \times \dots \times V_K \to W$ for $V_1,\dots,V_K,W \in \V$
is linear in each of its arguments.
As a special case we denote by the $0$-linear operators
into the function space $W$, simply the functions from $W$.%
\footnote{
This is motivated by the following relation:
Any $K$-linear operator $T: V_1 \times \dots \times V_K \to W$
can also be written as on operator
$\hat{T} : \Omega \times V_1\times  \dots \times V_K \to R(W)$
which is linear in the last $K$ arguments and defined by
$\hat{T}(\cdot, v_1,\dots,v_K) = T(v_1,\dots,v_K) \in W$.
The formal transition from $\hat{T}$ to $T$
is known as (partial) currying or Schönfinkelisation.
}

The number of arguments $K$ is called the arity of $T$.
In the following we will only consider
nullary operators ($K=0$, aka functions),
unary operators ($K=1$, linear operators), and
binary operators ($K=2$, bilinear operators).
For an operator $T:V_1 \times \dots \times V_K \to W$
the range space is the functions space $W$.
Since the range space of the latter will be used
frequently we use the notation
$\Rcal(T) = R(W)$ for this range of the range.

Examples for such operators are,
(assuming sufficient smoothness in all cases):
\begin{itemize}
  \item
    Any fixed function $f:\Omega \to R$ with $R \in \T$
    (e.g. $f \in V$ for some function space $V\in\V$)
    is a $0$-linear (or nullary) operator.
  \item
    The identity operator $Id:V \to V$ mapping some $V \in \V$ to itself
    is a $1$-linear (or unary) operator.
  \item
    The operator $V \ni v \mapsto fv \in W$
    mapping a function $v$ to the \emph{pointwise}
    product with some fixed function $f$
    is a $1$-linear (or unary) operator.
  \item
    The differential operators $\nabla, \div, \operatorname{curl}, \Delta$
    mapping sufficiently smooth function to functions
    are $1$-linear (or unary) operators.
  \item
    The operator $V_1 \times V_2 \ni (v_1,v_2) \mapsto v_1v_2 \in W$
    mapping two functions $v$ to their \emph{pointwise} product
    is a $2$-linear (or binary) operators.
  \item
    The operator $V_1 \times V_2 \ni (v_1,v_2) \mapsto \nabla v_1 \cdot \nabla v_2 \in W$
    mapping two functions $v$ to the \emph{pointwise} euclidean product of their gradients
    is a $2$-linear (or binary) operators.
\end{itemize}

These examples demonstrate an important construction principle:
Given a $K$-linear operator $T$ and an $L$-linear operator $S$
we can obtain a $(K+L)$-linear operator by taking some pointwise product.
To make this more precise, assume that we have $K$- and $L$-linear operators
\begin{align}
\label{eq:first_operators}
  T : U_1 \times \dots \times U_K & \to W_1,\\
\label{eq:second_operators}
  S : V_1 \times \dots \times V_L & \to W_2
\end{align}
and a local bilinear contraction $\Phi:\Rcal(T) \times \Rcal(S) \to R(W)$
where $U_1,\dots,U_{K}$,$V_1,\dots,V_L$, and  $W_1,W_2,W$
are appropriate function spaces from $\V$.
Then we define their contracted tensor product
\begin{align}\label{eq:operator_product}
  T \otimes_\Phi S :
    U_1 \times U_{K} \times V_1 \times V_L \to W
\end{align}
by
\begin{align*}
  (T \otimes_\Phi S)(u_1,\dots,u_K,v_1, \dots, v_L)
    = \Phi(T(u_1,\dots,u_K), S(v_1, \dots, v_L)).
\end{align*}
This is obviously a $(K+L)$-linear operator, where $\Phi$ computes a pointwise
product in the range spaces.

We now consider an example:
Assume that fixed functions $f:\Omega \to \R$ and $\alpha:\Omega \to \R^{d \times d}$
and a sufficiently smooth function space $V\in\V_\R$ are given.
Denote by $Id_V$ the unary identity on $V\in \V$
and by $D_V,D_U=\nabla$ the unary gradient operators on $V\in \V$ and $U\in \V$, respectively,
and identify $f$ and $\alpha$ with the nullary operators $F$ and $A$.
Furthermore denote by
$\Phi:\R^{d\times d} \times \R^d \to \R^d$ with $\Phi(M,x)=Mx$
the matrix-vector product and by
$\Psi:\R^{d} \times \R^d \to \R$ with $\Psi(x,y)=x\cdot y$
the euclidean inner product.
Then the bilinear and linear form
from \eqref{eq:poisson} can be written as
\begin{align*}
  a(u,v) &= \int_\Omega \bigl((A\otimes_\Phi D_U) \otimes_\Psi D_V\bigr)(u,v)(x)\, dx,\\
  \ell(v) &= \int_\Omega \bigl(F \otimes_\Psi Id_V)(v)(x)\, dx,
\end{align*}
i.e. the bilinear form  $a(\cdot,\cdot):U \times V \to \R$ is obtained by integrating
(pointwise for each argument pair $(u,v)$)
the bilinear operator
\begin{align*}
  \bigl((A\otimes_\Phi D_U) \otimes_\Psi D_V\bigr) : U \times V \to L^1(\Omega).
\end{align*}
Likewise the linear form $\ell(\cdot):V \to \R$ is obtained by integrating
(pointwise for each argument $v$)
the linear/unary operator
\begin{align*}
  \bigl(F \otimes_\Psi Id_V\bigr) : V \to L^1(\Omega).
\end{align*}

Another class of frequently used operations are linear range transformations.
Given a $K$-linear operator $T$ as in \eqref{eq:first_operators}
with range $\Rcal(T)$
and a linear map $\psi:\Rcal(T) \to R$ for some $R\in \T$
we consider the pointwise composed $K$-linear operator
\begin{align*}
  T_\psi : V_1 \times \dots \times V_K \to \hat{W}
\end{align*}
where $R(\hat{W}) = R$ and
\begin{align*}
  T_\psi(v_1,\dots, v_K )(x) =\psi(T(v_1,\dots,v_K)(x)) \qquad \forall x \in \Omega.
\end{align*}
Notice that, formally, this is not the composition of $\psi$ with $T$,
but the composition $T_\psi = \Psi\circ T$ of the superposition operator
$\Psi$ induced by $\psi$ with $T$.




\subsection{Elementary nullary operators}

The interface of a nullary operator extends the \cpp{GridViewFunction}
interface of the \dunemodule{dune-functions} module---i.e. a function that can be localized to elements of a
\cpp{GridView}.
The extension allows to encapsulate the quadrature requirements of such
\cpp{GridViewFunction}'s.
A \cpp{GridViewFunction} can be turned into a nullary operator
by providing this additional information in terms of
%a \cpp{QuadratureRuleKey}
the required quadrature order using
using:
\begin{lstlisting}[style=Example]
auto f = Coefficient(gridViewFunction, order);
\end{lstlisting}
The resulting object \cpp{f} is a nullary operator.
Notice that the interface name \cpp{Coefficient} reflects the
fact that such functions often arise as coefficients that do not depend
on ansatz and test functions.
The default value for \cpp{order} is $0$.
If the function is not a \cpp{GridViewFunction} but given
in terms of an analytic expression, it can be turned into
a \cpp{GridViewFunction} using the utilities provided
by the \dunemodule{dune-functions} module:
\begin{lstlisting}[style=Example]
auto func = [](auto x) { return x[0]*x[0]; };
auto f = Coefficient(Dune::Functions::makeGridViewFunction(func, gridView));
\end{lstlisting}
Here we made use of the fact that passing a temporary \cpp{GridViewFunction}
since \cpp{Coefficient} will store it by value.
\IMPtodo{
Allow to pass \texttt{QuadratureRuleKey}
instead of order.
}



\subsection{Elementary unary operators}

Unary operators basically represent linear differential operators
on finite element spaces and e.g. be used to construct bilinear operators
using multiplication. Following the notation used above, the first
argument of a bilinear form represents the ansatz or trial space,
while the second one represents the test space.
Since it is often convenient not be forced to use the ansatz space
as first factor during multiplication, the role of ansatz and test
space has to be manually defined by the user.

The most simple (zero-order) differential operator on a finite element
space is the identity operator. For a finite element space
respresented by a \cpp{GlobalBasis} in the sense of the
\dunemodule{dune-functions} module this operator
can be obtained using
\begin{lstlisting}[style=Example]
auto u = trialFunction(basis);
auto v = testFunction(basis);
\end{lstlisting}
By using either the \cpp{trialFunction} or \cpp{testFunction}
method, the role of the corresponding space is encoded into the
resulting object and preserved when constructing derived operators
(see below).

Given an unary identity operator \cpp{u}
the associated first order differential operators $D, \nabla, \div$
can be obtained using
\begin{lstlisting}[style=Example]
auto u     = trialFunction(basis);
auto DU    = jacobian(u);
auto gradU = grad(u);
auto divU  = div(u);
\end{lstlisting}
Notice that the range of \cpp{jacobian(u)}
is always represented using a \cppbreak{Dune::FieldMatrix<T,N,M>}
Hence, both this is only supported for function spaces
where $Du$ and $\nabla u$ can be represented by such matrices, i.e.
where $u$ is either a leaf in the ansatz tree with scalar or vector
range, or a power space (see above) where the leaf ansatz space
has a scalar range.
\cpp{grad(u)} simply transposes the Jacobian matrices
and hence the same restrictions,
while \cpp{div(u)} is restricted to cases,
where \cpp{jacobian(u)} has a square matrix range.

It is important to understand that in the above cases
\cpp{u}, \cpp{jacobian(u)}, \cpp{grad(u)}, \cpp{div(u)}
formally do not represent a specific function (or its Jacobian, ...),
but the gradient operator on the subspace associated to \cpp{u}.

If the operator shall only depend on a subspace of a nested basis,
this can be achieved by passing either
a \cpp{Dune::Functions::SubspaceBasis},
\begin{lstlisting}[style=Example]
using namespace Dune::Indices;
auto u = trialFunction(Dune::Functions::subspaceBasis(taylorHoodBasis, _0));
auto p = trialFunction(Dune::Functions::subspaceBasis(taylorHoodBasis, _1));
\end{lstlisting}
a \cpp{GlobalBasis} together with a tree path object,
\begin{lstlisting}[style=Example]
using namespace Dune::Indices;
auto u = trialFunction(taylorHoodBasis, Dune::TypeTree::treePath(_0));
auto p = trialFunction(taylorHoodBasis, Dune::TypeTree::treePath(_1));
\end{lstlisting}
or a \cpp{GlobalBasis} together with one or several plain tree path indices
\begin{lstlisting}[style=Example]
using namespace Dune::Indices;
auto u = trialFunction(taylorHoodBasis, _0);
auto p = trialFunction(taylorHoodBasis, _1);
\end{lstlisting}
Notice that it is currently only supported that the addressed
subspace belongs to a leaf in the ansatz tree or to a power space
of leaf spaces (i.e. one level below the leafs).
Even if the operator depends on the subspace only, it is still
considered to be on operator of on the full space that is invariant
under the contributions from other subspaces.



\subsection{Products of operators}

Given a $K$-linear operator $T$ as in \eqref{eq:first_operators},
an $L$-linear operator $S$ as in \eqref{eq:second_operators},
and a bilinear range contraction $\Phi$
we can construct the contracted tensor product
$T\otimes_\Phi S$ as in \eqref{eq:operator_product}.
In the user interface such a product can be constructed using
\begin{lstlisting}[style=Example]
auto A = product(Phi, T, S);
\end{lstlisting}
Here, the operands \cpp{T} and \cpp{S} should
satisfying either of the following assumptions:
\begin{itemize}
  \item
    Both are nullary operators.
    Then \cpp{A} is again nullary.
  \item
    One of them is nullary while the other
    is unary. Then \cpp{A} is unary.
    The internal role as test- or trial-space
    of the unary operator is preserved in \cpp{A}.
  \item
    Both of them are unary and exactly one
    is marked as test- and one as trial-space.
    Then \cpp{A} is binary and uses the
    \cpp{T} and \cpp{S} according
    to their test- trial-space role.
  \item
    One of them is a nullary, unary, or binary operator
    and the other one is a plain value.
    Then \cpp{A} preserves the arity and test- or trial-space role
    of the operator provided as argument.
\end{itemize}
The contraction \cpp{Phi} is a binary callback function
that allows to pass elements from the range spaces
$\Rcal$(\cpp{T}) and $\Rcal$(\cpp{S}) as first and second
argument, respectively.

For convenience, there are two predefined product operations.
Using the plain multiplication operator \cpp{*} corresponds
to using the scalar-vector, scalar-matrix, matrix-vector,
or matrix-matrix product:
\begin{lstlisting}[style=Example]
auto A = T*S;
\end{lstlisting}
Using the \cpp{dot} function
corresponds to using the Euclidean inner product
for vectors and the Frobenius inner product for matrices:
\begin{lstlisting}[style=Example]
auto A = dot(T,S);
\end{lstlisting}
Internally, these special cases are implemented
using the contractions \cpp{Impl::MultOp} and
\cpp{Impl::DotOp}.

Notice that the arity of a product operator is always
the sum of the arities of the operands. It is also possible
to nest several products as long as the arity of the outer most
one does not exceed 2. For example one can use:
\begin{lstlisting}[style=Example]
auto u = trialFunction(basis);
auto v = testFunction(basis);
auto alpha = Coefficient(matrixValuedGridFunction);
auto A = dot(alpha*grad(u),grad(v));
\end{lstlisting}

It is perfectly fine (and necessary in many applications)
if, in the product of two unary operators, both factors
depend on different subspaces of the full trial- or test-space.
This is e.g. the case when defining the operators
for a weak divergence constrained on a Taylor--Hood space:
\begin{lstlisting}[style=Example]
using namespace Dune::Indices;
auto u = trialFunction(taylorHoodBasis, _0);
auto p = trialFunction(taylorHoodBasis, _1);
auto v = testFunction(taylorHoodBasis, _0);
auto q = testFunction(taylorHoodBasis, _1);
auto B = div(u)*q;
auto BT = div(v)*p;
\end{lstlisting}



\subsection{Pointwise composition and superposition operators}

Linear range transformations $T_\psi$ of $T$ by some local linear $\psi$
are obtained by pointwise composition of $\psi$ with $T$.
This can be achieved using
\begin{lstlisting}[style=Example]
auto A = compose(psi, T);
\end{lstlisting}
Here, the operand \cpp{T} should
be a nullary, unary, or binary operator
and the transformation \cpp{phi} is a linear unary callback function
that allows to pass elements from the range spaces
$\Rcal$(\cpp{T}).

There are a few predefined transformations:
\begin{lstlisting}[style=Example]
auto AT = transpose(T);
\end{lstlisting}
transposes the values in the range $\Rcal$(\cpp{T}),
\begin{lstlisting}[style=Example]
auto S = symmetrize(T);
\end{lstlisting}
computes the symmetric part for a matrix range $\Rcal$(\cpp{T}),
\begin{lstlisting}[style=Example]
auto tr = trace(T);
\end{lstlisting}
computes the trace for a matrix range $\Rcal$(\cpp{T}).

To allow for a nicer syntax for custom range transformations,
a local transformation \cpp{psi} can be turned into the
associated superposition operator which can directly be
chained with multilinear operators:
\begin{lstlisting}[style=Example]
auto Psi = RangeOperator(psi);
auto A = Psi(T);
\end{lstlisting}
Here the result is the same as for \cpp{compose(psi, T)}.
For convenience, \cpp{Psi} can still be applied to plain
(non-operator) values yielding the same result as \cpp{psi},
i.e. it can be used as a replacement of \cpp{psi}
that additionally allows to pass operators.
\IMPdiscuss{
Rename \texttt{RangeOperator} to, e.g., \texttt{ComposableOperator}
or \texttt{SuperpositionOperator}?
}



\subsection{Sums of operators}

In applications one frequently encounters the case that the
bilinear form is given by an integral over a sum of multiple
bilinear operators. This is e.g. the case for the Stokes problem
with the bilinear form
\begin{align*}
  ((u,p), (v,q)) \mapsto \int_\Omega \nabla u : \nabla v + q\nabla \cdot u + p \nabla \cdot v
\end{align*}
where $(u,p)$ and $(v,q)$ are from the trial- and test-space, respectively.

In the user interface, sums and differences of operators can easily be
defined using \cpp{operator+} and \cpp{operator-}.
\begin{lstlisting}[style=Example]
auto A = T+S;
auto B = T-S;
\end{lstlisting}
In order for these operations to make sense, the operators
\cpp{T} and \cpp{S}
are required to satisfy the following assumptions:
\begin{itemize}
  \item
    The range type $\Rcal$(\cpp{T}) and $\Rcal$(\cpp{S})
    are interoperable.
  \item
    \cpp{T} and \cpp{S} have the same arity.
  \item
    If \cpp{T} and \cpp{S} are both unary, they are either
    both acting on the trial-space or both on the test-space.
\end{itemize}

It is also possible to use sums inside of products and
linear range transformations.
Such expressions will internally be translated to a sum of products.
E.g.
\begin{lstlisting}[style=Example]
auto A = transpose(T+S)*R;
\end{lstlisting}
is internally translated to
\begin{lstlisting}[style=Example]
auto A = transpose(T)*R + transpose(S)*R;
\end{lstlisting}



\subsection{Integrating operators}

So far we described how to compose $K$-linear operators
on functions spaces.
Such operators can be transformed to $K$-linear forms
by integration using some measure.
Currently two types of measure are supported.
\begin{lstlisting}[style=Example]
auto a = integrate(A);
\end{lstlisting}
integrates \cpp{A} using a bulk integral over the domain, whereas
\begin{lstlisting}[style=Example]
auto a = integrate(A, boundaryPatch);
\end{lstlisting}
uses a boundary integral over the given \cpp{boundaryPatch}.
The operator \cpp{A} has to be either unary or binary
corresponding to a linear or bilinear form, respectively.

In any case the resulting object \cpp{a} provides the interface
of a local bulk assembler and can be passed to the corresponding
global assembler which is either a
\cppbreak{Dune::Fufem::DuneFunctionsFunctionalAssembler}
for linear forms or a \cppbreak{Dune::Fufem::DuneFunctionsOperatorAssembler}
for a bilinear form.

When integrating over a grid element this local assembler
determines a suitable quadrature rule for each summand independently,
to benefit from lower order terms.
Evaluations of shape function values and derivatives
are in general cached as far as possible.
This includes sharing evaluations of shape functions
across different subexpressions.
In general evaluation of shape function values and local
derivatives is also shared across different elements of the
same element type. For non-affine families of finite elements
(e.g. in case of Raviart--Thomas- or BDM-elements, or local $p$-adaptivity)
this behavior can be disabled by explicitly passing the
additional tag argument.
\begin{lstlisting}[style=Example]
auto a = integrate(A, NonAffineFamiliyTag{});
\end{lstlisting}

So far it is not possible to detect non-affine families
automatically. Furthermore, it is also not possible, to set
the flag only for subspaces in the ansatz tree.
\IMPtodo{
Implement automatic detection of affine families,
e.g. using explicit specialization as long as the information
is not provided by the basis.
}
\IMPtodo{
Allow to change the is-affine property per  node.
}



\subsection{Binding operators to coefficients}

While an unary operator does not represent a function, but a linear operator
$A:V \to W$ between two function spaces,
it is needed to apply it to a specific function from the domain
function space $V$.
Since the unary operators defined described here,
are by construction associated to basis of the function space $V$,
we can identify functions from $V$ by a coefficient vector
with respect to this basis.
Given a coefficient vector $c$ of a function $v\in V$,
the function $w = A v$ is obtained by binding the unary operator
to $c$.
\begin{lstlisting}[style=Example]
auto w = bindToLocalView(A, c);
\end{lstlisting}
Given an unary operator \cpp{A} and a coefficient vector \cpp{c}
suitable for the underlying global basis, this returns a nullary
operator that can e.g. be used as coefficient.
Since nullary operators implement the \cpp{GridViewFunction} interface,
the resulting object can e.g. also be used to write the
function \cpp{w} using the \cpp{Dune::VTKWriter}.
\begin{lstlisting}[style=Example]
using namespace Dune::Indices;
// Assume that x is a coefficient vector for taylorHoodBasis
auto u = trialFunction(taylorHoodBasis, _0);
auto p = trialFunction(taylorHoodBasis, _1);

auto vtkWriter = Dune::VTKWriter(taylorHoodBasis.gridView());
vtkWriter.addVertexData(
  bindToCoefficients(u, x),
  VTK::FieldInfo("velocity", VTK::FieldInfo::Type::vector, dim));
vtkWriter.addVertexData(
  bindToCoefficients(p, x),
  VTK::FieldInfo("pressure", VTK::FieldInfo::Type::scalar, 1));
vtkWriter.addVertexData(
  bindToCoefficients(div(u), x),
  VTK::FieldInfo("divergence", VTK::FieldInfo::Type::scalar, 1));
vtkWriter.addVertexData(
  bindToCoefficients(grad(p), x),
  VTK::FieldInfo("pressuregradient", VTK::FieldInfo::Type::vector, dim));
vtkWriter.write("stokes_solution");
\end{lstlisting}
\IMPdiscuss{%
Maybe overloading an operator wuld be nice,
e.g. \cpp{div(u) << x} or \cpp{(div(u),x)}.
While I prefer the former, it's prohibited,
by a far to greedy overload in dune-common.
}%



\subsection{Examples}

In this section we present some examples on how to use the
building blocks introduced above to define linear and bilinear forms
for several partial differential equations.

\paragraph{Poisson equation}

Consider the Poisson-type problem
\begin{align*}
  -\div(\alpha \nabla u) &= f
    \quad \text{in }\Omega, &
  u &= u_D
    \quad \text{on }\Gamma_D, &
  \beta u + \alpha \frac{du}{d\nu} &= g
    \quad \text{on }\Gamma_R.
\end{align*}
on $\Omega \subset \R^d$,
Dirichlet boundary values on $\Gamma_D \subset \partial \Omega$,
and Robin boundary values on $\Gamma_R \subset \partial \Omega$.
For simplicity assume that the diffusivity matrix $\alpha \in \R^{d \times d}$
and the factor $\beta>0$ are constant
(although we could easily use spatial dependence here)
while $f \in L^2(\Omega)$ and $g \in L^2(\Gamma_R)$
are given functions.
The weak form of this problem is
\begin{align*}
  u \in H^1_{\Gamma_D,u_D}:
  \qquad
  a(u,v) = \ell(v)
  \qquad \forall v \in H^1_{\Gamma_D,0}
\end{align*}
on the spaces $H^1_{\Gamma_D, r}(\Omega) = \{v \in H^1(\Omega) \st v|_{\Gamma_D} = r\}$
with the bilinear form $a(\cdot, \cdot)$ and the linear form $\ell(\cdot)$
given by
\begin{align*}
  a(u,v) &= \int_\Omega \alpha \nabla u \cdot \nabla v\, dx + \int_{\Gamma_R} \beta u v\, ds, \\
  \ell(v) &= \int_\Omega fv\, dx + \int_{\Gamma_R} g v\, ds.
\end{align*}
Assuming that this problem is discretized with Lagrange-finite elements of order 2,
the bilinear form and linear form can be defined by:
\begin{lstlisting}[style=Example]
// Assume that the following is given:
// the gridView the discretization should live on,
// the rightHandSide as GridViewFunction,
// the robinValues as GridViewFunction,
// the number beta as double,
// the matrix alpha as Dune:FieldMatrix<double,dim,dim>,
// the robinBoundary as BoundaryPath

using namespace Dune::Functions::BasisFactory;

// Create the basis
auto basis = makeBasis(gridView, lagrange<2>());

// Define trial and test functions
auto u = trialFunction(basis);
auto v = testFunction(basis);

// Define coefficient functions
auto f = Coefficient(rightHandSide, 1);
auto g = Coefficient(robinValues, 1);

// Define bilinear and linear form
auto a = integrate(dot(alpha*grad(u), grad(v)))
       + integrate(beta*u*v, robinBoundary);
auto b = integrate(f*v) + integrate(g*v, robinBoundary);
\end{lstlisting}

For the following examples we will start from the weak form directly
and omit boundary conditions.

\paragraph{Stokes problem}

Now we consider the Stokes problem.
Its weak form is given by: Find
$u \in H^1_0(\Omega)^d$ and $p \in L^2_0(\Omega)$ such that
\begin{alignat*}{3}
  &a(u,v) + b(v,p) &&= \ell(v) \qquad &&\forall v\in H^1_0(\Omega)^d,\\
  &b(u,q)          &&= 0 \qquad &&\forall q\in L^2_0(\Omega).
\end{alignat*}
where
\begin{align*}
  a(u,v) &= \int_\Omega \nabla u : \nabla v\, dx, &
  b(u,q) &= \int_\Omega \div(u)q\, dx, &
  \ell(v) &= \int_\Omega f\cdot v\, dx.
\end{align*}
Alternatively we can formulate this as
\begin{align*}
  (u,p) \in H^1_0(\Omega)^d \times L^2_0(\Omega): 
  \qquad
  A((u,p),(v,q)) = \ell((v,q))
  \qquad \forall
  (v,q) \in H^1_0(\Omega)^d \times L^2_0(\Omega): 
\end{align*}
with a single bilinear form
\begin{align*}
  A((u,p),(v,q)) = \int_\Omega \nabla u : \nabla v + \div(u)q + \div(v)p \, dx.
\end{align*}
A discretization of this problem with lowest order Taylor--Hood finite elements
can be defined by:
\begin{lstlisting}[style=Example]
using namespace Dune::Indices;
using namespace Dune::Functions::BasisFactory;

// Create the basis
auto taylorHoodBasis = makeBasis(gridView,
        composite(
          power<dim>(lagrange<2>(), blockedInterleaved()),
          lagrange<1>()
        ));

// Define trial and test functions
auto u = trialFunction(taylorHoodBasis, _0);
auto p = trialFunction(taylorHoodBasis, _1);
auto v = testFunction(taylorHoodBasis, _0);
auto q = testFunction(taylorHoodBasis, _1);

// Define coefficient functions
auto f = Coefficient(rightHandSide, 2);

// Define bilinear and linear form
auto A = integrate( dot(grad(u), grad(v)) + div(u)*q + div(v)*p );
auto L = integrate(dot(f,v));
\end{lstlisting}



\bibliographystyle{plainnat}
\bibliography{dune-fufem}

\end{document}
