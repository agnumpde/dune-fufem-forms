// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <vector>
#include <cmath>

#include <dune/common/bitsetvector.hh>
#include <dune/common/transpose.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>

#include <dune/fufem/parallel/elementcoloring.hh>
#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/assemblers/dunefunctionsfunctionalassembler.hh>
#include <dune/fufem/backends/istlmatrixbackend.hh>
#include <dune/fufem/boundarypatch.hh>

#include <dune/fufem/forms/forms.hh>

using namespace Dune;


// Mark all DOFs associated to entities contained in
// the boundary patch.
template<class Basis, class Vector, class Patch>
void markPatchDOFs(const Basis& basis, Vector& vector, const Patch& patch)
{
  auto vectorBackend = Dune::Functions::istlVectorBackend(vector);
  vectorBackend.resize(basis);
  vectorBackend = 0;
  Dune::Functions::forEachBoundaryDOF(basis, [&] (auto&& localIndex, const auto& localView, const auto& intersection) {
    if (patch.contains(intersection))
      vectorBackend[localView.index(localIndex)] = true;
  });
}



template<int dim>
auto createUniformCubeGrid()
{
  using Grid = Dune::YaspGrid<dim>;
  Dune::FieldVector<double,dim> l(1.0);
  std::array<int,dim> elements = {{2, 2}};
  return std::make_unique<Grid>(l, elements);
}

#if HAVE_DUNE_UGGRID
auto createMixedGrid()
{
  using Grid = Dune::UGGrid<2>;
  Dune::GridFactory<Grid> factory;
  for(unsigned int k : Dune::range(9))
    factory.insertVertex({0.5*(k%3), 0.5*(k/3)});
  factory.insertElement(Dune::GeometryTypes::cube(2), {0, 1, 3, 4});
  factory.insertElement(Dune::GeometryTypes::cube(2), {1, 2, 4, 5});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {3, 4, 6});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 7, 6});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 5, 7});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {5, 8, 7});
  return std::unique_ptr<Grid>{factory.createGrid()};
}
#endif

int main (int argc, char *argv[]) try
{
  std::size_t threadCount = 4;
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));

  // Set up MPI, if available
  MPIHelper::instance(argc, argv);

  ///////////////////////////////////
  //   Generate the grid
  ///////////////////////////////////

#if HAVE_DUNE_UGGRID
  auto gridPtr = createMixedGrid();
#else
  auto gridPtr = createUniformCubeGrid<2>();
#endif

  auto& grid = *gridPtr;
  grid.globalRefine(4);
//  grid.globalRefine(8);

  auto gridView = grid.leafGridView();
  using GridView = decltype(gridView);

  /////////////////////////////////////////////////////////
  //   Choose a finite element space
  /////////////////////////////////////////////////////////

  typedef Functions::LagrangeBasis<GridView,2> FEBasis;
  FEBasis feBasis(gridView);

  /////////////////////////////////////////////////////////
  //   Stiffness matrix and right hand side vector
  /////////////////////////////////////////////////////////

  typedef BlockVector<FieldVector<double,1> > VectorType;
  typedef BCRSMatrix<FieldMatrix<double,1,1> > MatrixType;

  VectorType rhs;
  MatrixType stiffnessMatrix;

  /////////////////////////////////////////////////////////
  //  Assemble the system
  /////////////////////////////////////////////////////////

  std::cout << "Number of DOFs is " << feBasis.dimension() << std::endl;

  auto dirichletIndicatorFunction = [](auto x) { return true; };

  auto dirichletPatch = BoundaryPatch(feBasis.gridView());
  dirichletPatch.insertFacesByProperty([&](auto&& intersection) { return dirichletIndicatorFunction(intersection.geometry().center()); });

  auto rightHandSide = [] (const auto& x) { return 10;};
  auto pi = std::acos(-1.0);
  auto dirichletValues = [pi](const auto& x){ return std::sin(2*pi*x[0]); };

  auto gridViewPartition = Dune::Fufem::coloredGridViewPartition(gridView);

  {
    Dune::Timer timer;
    using namespace ::Dune::Fufem::Forms;
    namespace F = ::Dune::Fufem::Forms;

    auto v = testFunction(feBasis);
    auto u = trialFunction(feBasis);
    auto f = Coefficient(Functions::makeGridViewFunction(rightHandSide, gridView));

    auto a = integrate(dot(grad(u), grad(v)));
    auto b = integrate(f*v);

    auto operatorAssembler = Dune::Fufem::DuneFunctionsOperatorAssembler{feBasis, feBasis};
    auto matrixBackend = Dune::Fufem::istlMatrixBackend(stiffnessMatrix);

    timer.reset();
    operatorAssembler.assembleBulk(matrixBackend, a, gridViewPartition, threadCount);
    std::cout << "Assembling the matrix took " << timer.elapsed() << "s" << std::endl;

    timer.reset();
    auto functionalAssembler = Dune::Fufem::DuneFunctionsFunctionalAssembler{feBasis};
    auto rhsBackend = Dune::Functions::istlVectorBackend(rhs);
    functionalAssembler.assembleBulk(rhsBackend, b, gridViewPartition, threadCount);
    std::cout << "Assembling the rhs took " << timer.elapsed() << "s" << std::endl;
  }


  /////////////////////////////////////////////////
  //   Choose an initial iterate
  /////////////////////////////////////////////////
  VectorType x(feBasis.size());
  x = 0;

  // Determine Dirichlet dofs
  std::vector<char> dirichletNodes;



  markPatchDOFs(feBasis, dirichletNodes, dirichletPatch);


  // Interpolate dirichlet values at the boundary nodes
  {
    Dune::Timer timer;
    interpolate(feBasis, x, dirichletValues, dirichletNodes);
    std::cout << "Interpolating dirichlet values took " << timer.elapsed() << "s" << std::endl;
  }

  //////////////////////////////////////////////////////
  //   Incorporate dirichlet values in a symmetric way
  //////////////////////////////////////////////////////

  // Compute residual for non-homogeneous Dirichlet values
  // stored in x. Since x is zero for non-Dirichlet DOFs,
  // we can simply multiply by the matrix.
  stiffnessMatrix.mmv(x, rhs);

  // Change Dirichlet matrix rows and columns to the identity.
  for (size_t i=0; i<stiffnessMatrix.N(); i++) {
    if (dirichletNodes[i]) {
      rhs[i] = x[i];
      auto cIt    = stiffnessMatrix[i].begin();
      auto cEndIt = stiffnessMatrix[i].end();
      // loop over nonzero matrix entries in current row
      for (; cIt!=cEndIt; ++cIt)
        *cIt = (i==cIt.index()) ? 1 : 0;
    }
    else {
      auto cIt    = stiffnessMatrix[i].begin();
      auto cEndIt = stiffnessMatrix[i].end();
      // loop over nonzero matrix entries in current row
      for (; cIt!=cEndIt; ++cIt)
        if (dirichletNodes[cIt.index()])
          *cIt = 0;
    }
  }

  ////////////////////////////
  //   Compute solution
  ////////////////////////////

  // Technicality:  turn the matrix into a linear operator
  MatrixAdapter<MatrixType,VectorType,VectorType> op(stiffnessMatrix);

  // Sequential incomplete LU decomposition as the preconditioner
  SeqILDL<MatrixType,VectorType,VectorType> ildl(stiffnessMatrix,1.0);

  // Preconditioned conjugate-gradient solver
  CGSolver<VectorType> cg(op,
                          ildl, // preconditioner
                          1e-4, // desired residual reduction factor
                          100,   // maximum number of iterations
                          2);   // verbosity of the solver

  // Object storing some statistics about the solving process
  InverseOperatorResult statistics;

  // Solve!
  cg.apply(x, rhs, statistics);

  ////////////////////////////////////////////////////////////////////////////
  //  Make a discrete function from the FE basis and the coefficient vector
  ////////////////////////////////////////////////////////////////////////////

  auto xFunction = Functions::makeDiscreteGlobalBasisFunction<double>(feBasis, x);

  //////////////////////////////////////////////////////////////////////////////////////////////
  //  Write result to VTK file
  //  We need to subsample, because VTK cannot natively display real second-order functions
  //////////////////////////////////////////////////////////////////////////////////////////////
  SubsamplingVTKWriter<GridView> vtkWriter(gridView, Dune::refinementLevels(2));
  vtkWriter.addVertexData(xFunction, VTK::FieldInfo("x", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.write("poisson-pq2");

 }
// Error handling
 catch (Exception& e) {
    std::cout << e.what() << std::endl;
 }
