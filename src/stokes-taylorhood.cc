// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <array>
#include <vector>

#include <dune/common/bitsetvector.hh>
#include <dune/common/indices.hh>
#include <dune/common/transpose.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/grid/utility/structuredgridfactory.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/multitypeblockmatrix.hh>
#include <dune/istl/multitypeblockvector.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/taylorhoodbasis.hh>
#include <dune/functions/backends/istlvectorbackend.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/subspacebasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>

#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>

#include <dune/fufem/parallel/elementcoloring.hh>
#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/assemblers/dunefunctionsfunctionalassembler.hh>
#include <dune/fufem/backends/istlmatrixbackend.hh>

#include <dune/fufem/forms/forms.hh>



#define BLOCKEDBASIS 1

// { using_namespace_dune_begin }
using namespace Dune;
// { using_namespace_dune_end }



// { main_begin }
int main (int argc, char *argv[]) try
{
  std::size_t threadCount = 4;
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));



  // Set up MPI, if available
  MPIHelper::instance(argc, argv);
  // { mpi_setup_end }

  ///////////////////////////////////
  //   Generate the grid
  ///////////////////////////////////

  // { grid_setup_begin }
  const int dim = 2;
  using GridType = YaspGrid<dim>;
  FieldVector<double,dim> upperRight = {1, 1};
//  std::array<int,dim> elements = {{400, 400}};
  std::array<int,dim> elements = {{4, 4}};
  GridType grid(upperRight,elements);

  using GridView = typename GridType::LeafGridView;
  GridView gridView = grid.leafGridView();
  // { grid_setup_end }

  /////////////////////////////////////////////////////////
  //   Choose a finite element space
  /////////////////////////////////////////////////////////

#if BLOCKEDBASIS
  // { function_space_basis_begin }
  using namespace Functions::BasisFactory;

  constexpr std::size_t K = 1; // pressure order for Taylor-Hood

  auto taylorHoodBasis = makeBasis(
          gridView,
          composite(
            power<dim>(
              lagrange<K+1>(),
              blockedInterleaved()),
            lagrange<K>()
          ));
  // { function_space_basis_end }
#else
  using namespace Functions::BasisFactory;

  static const std::size_t K = 1; // pressure order for Taylor-Hood
  auto taylorHoodBasis = makeBasis(
          gridView,
          composite(
            power<dim>(
              lagrange<K+1>(),
              flatInterleaved()),
            lagrange<K>()
          ));
#endif



  /////////////////////////////////////////////////////////
  //   Stiffness matrix and right hand side vector
  /////////////////////////////////////////////////////////

#if BLOCKEDBASIS
  // { linear_algebra_setup_begin }
  using VelocityVector = BlockVector<FieldVector<double,dim>>;
  using PressureVector = BlockVector<FieldVector<double,1>>;
  using VectorType = MultiTypeBlockVector<VelocityVector, PressureVector>;

  using VelocityBitVector = BlockVector<FieldVector<char,dim>>;
  using PressureBitVector = BlockVector<FieldVector<char,1>>;
  using BitVectorType = MultiTypeBlockVector<VelocityBitVector, PressureBitVector>;

  using Matrix00 = BCRSMatrix<FieldMatrix<double,dim,dim>>;
  using Matrix01 = BCRSMatrix<Dune::Fufem::SingleColumnMatrix<FieldMatrix<double,dim,1>>>;
  using Matrix10 = BCRSMatrix<Dune::Fufem::SingleRowMatrix<FieldMatrix<double,1,dim>>>;
  using Matrix11 = BCRSMatrix<FieldMatrix<double,1,1>>;   /*@\label{li:matrix_type_pressure_pressure}@*/
  using MatrixRow0 = MultiTypeBlockVector<Matrix00, Matrix01>;
  using MatrixRow1 = MultiTypeBlockVector<Matrix10, Matrix11>;
  using MatrixType = MultiTypeBlockMatrix<MatrixRow0,MatrixRow1>;
  // { linear_algebra_setup_end }
#else
  using VectorType = BlockVector<BlockVector<FieldVector<double,1> > >;
  using BitVectorType = BlockVector<BlockVector<FieldVector<char,1> > >;
  using MatrixType = Matrix<BCRSMatrix<FieldMatrix<double,1,1> > >;
#endif

  /////////////////////////////////////////////////////////
  //  Assemble the system
  /////////////////////////////////////////////////////////

  // { rhs_assembly_begin }
  VectorType rhs;

  auto rhsBackend = Dune::Functions::istlVectorBackend(rhs);

  rhsBackend.resize(taylorHoodBasis);
  rhs = 0;                                 /*@\label{li:stokes_taylorhood_set_rhs_to_zero}@*/
  // { rhs_assembly_end }

  // { matrix_assembly_begin }
  MatrixType stiffnessMatrix;

  auto coloredPartition = Dune::Fufem::coloredGridViewPartition(gridView);

  {
    Dune::Timer timer;
    using namespace Indices;
    using namespace ::Dune::Fufem::Forms;
    auto velocityBasis = Functions::subspaceBasis(taylorHoodBasis, _0);
    auto pressureBasis = Functions::subspaceBasis(taylorHoodBasis, _1);

    auto u = trialFunction(velocityBasis);
    auto p = trialFunction(pressureBasis);
    auto q = testFunction(pressureBasis);
    auto v = testFunction(velocityBasis);

    auto A = integrate( dot(grad(u), grad(v)) + div(u)*q + div(v)*p );

    auto operatorAssembler = Dune::Fufem::DuneFunctionsOperatorAssembler{taylorHoodBasis, taylorHoodBasis};
    auto matrixBackend = Dune::Fufem::istlMatrixBackend(stiffnessMatrix);

    std::cout << "Setting up local assembler and matrix backend took " << timer.elapsed() << "s" << std::endl;
    timer.reset();

    {
      Dune::Timer timer;

      auto patternBuilder = matrixBackend.patternBuilder();
      operatorAssembler.assembleBulkPattern(patternBuilder, coloredPartition, threadCount);

      std::cout << "assembleBulkPattern took " << timer.elapsed() << "s." << std::endl;
      timer.reset();

      patternBuilder.setupMatrix();
      matrixBackend.assign(0);

      std::cout << "setupMatrix took " << timer.elapsed() << "s." << std::endl;
      timer.reset();

      operatorAssembler.assembleBulkEntries(matrixBackend, A, coloredPartition, threadCount);

      std::cout << "assembleBulkEntries took " << timer.elapsed() << "s." << std::endl;
    }

    std::cout << "Assembling the problem took " << timer.elapsed() << "s" << std::endl;
  }

  // { matrix_assembly_end }

  /////////////////////////////////////////////////////////
  // Set Dirichlet values.
  // Only velocity components have Dirichlet boundary values
  /////////////////////////////////////////////////////////

  // { initialize_boundary_dofs_vector_begin }

  BitVectorType isBoundary;

  auto isBoundaryBackend = Dune::Functions::istlVectorBackend(isBoundary);
  isBoundaryBackend.resize(taylorHoodBasis);
  isBoundary = false;
  // { initialize_boundary_dofs_vector_end }

  // { determine_boundary_dofs_begin }
  using namespace Indices;
  Functions::forEachBoundaryDOF(
          Functions::subspaceBasis(taylorHoodBasis, _0),
          [&] (auto&& index) {
            isBoundaryBackend[index] = true;
          });
  // { determine_boundary_dofs_end }

  // { interpolate_dirichlet_values_begin }
  using Coordinate = GridView::Codim<0> ::Geometry::GlobalCoordinate;
  using VelocityRange = FieldVector<double,dim>;
  auto&& velocityDirichletData = [](Coordinate x)
  {
    return VelocityRange{0.0, double(x[0] < 1e-8)};
  };

  Functions::interpolate(
          Functions::subspaceBasis(taylorHoodBasis, _0), rhs,
          velocityDirichletData,
          isBoundary);
  // { interpolate_dirichlet_values_end }

  ////////////////////////////////////////////
  //   Modify Dirichlet rows
  ////////////////////////////////////////////

  auto matrixBackend = Dune::Fufem::istlMatrixBackend(stiffnessMatrix);

  // loop over the matrix rows
  // { set_dirichlet_matrix_begin }
  auto localView = taylorHoodBasis.localView();
  for(const auto& element : Dune::elements(taylorHoodBasis.gridView()))
  {
    localView.bind(element);
    for (size_t i=0; i<localView.size(); ++i)
    {
      auto row = localView.index(i);
      // If row corresponds to a boundary entry, modify
      // it to be an identity matrix row
      if (isBoundaryBackend[row])
        for (size_t j=0; j<localView.size(); ++j)
        {
          auto col = localView.index(j);
          matrixBackend(row, col) = (i==j) ? 1 : 0; 
        }
    }
  }
  // { set_dirichlet_matrix_end }

  ////////////////////////////
  //   Compute solution
  ////////////////////////////
  // { stokes_solve_begin }
  // Start from the rhs vector; that way the Dirichlet entries are already correct
  VectorType x = rhs;

  // Technicality:  turn the matrix into a linear operator
  MatrixAdapter<MatrixType,VectorType,VectorType> stiffnessOperator(stiffnessMatrix);

  // Fancy (but only) way to not have a preconditioner at all
  Richardson<VectorType,VectorType> preconditioner(1.0);

  // Construct the actual iterative solver
  RestartedGMResSolver<VectorType> solver(
          stiffnessOperator,  // operator to invert
          preconditioner,     // preconditioner for iteration
          1e-10,              // desired residual reduction factor
          500,                // number of iterations between restarts
          500,                // maximum number of iterations
          2);                 // verbosity of the solver

  // Object storing some statistics about the solving process
  InverseOperatorResult statistics;

  // Solve!
  solver.apply(x, rhs, statistics);
  // { stokes_solve_end }

  ////////////////////////////////////////////////////////////////////////////
  //  Make a discrete function from the FE basis and the coefficient vector
  ////////////////////////////////////////////////////////////////////////////

  // { make_result_functions_begin }
//  using VelocityRange = FieldVector<double,dim>;
//  using PressureRange = double;

//  auto velocityFunction
//          = Functions::makeDiscreteGlobalBasisFunction<VelocityRange>(
//            Functions::subspaceBasis(taylorHoodBasis, _0), x);
//  auto pressureFunction
//          = Functions::makeDiscreteGlobalBasisFunction<PressureRange>(
//            Functions::subspaceBasis(taylorHoodBasis, _1), x);

  using namespace Dune::Fufem::Forms;
  auto u = trialFunction(taylorHoodBasis, _0);
  auto p = trialFunction(taylorHoodBasis, _1);

  // { make_result_functions_end }

  //////////////////////////////////////////////////////////////////////////////////////////////
  //  Write result to VTK file
  //  We need to subsample, because VTK cannot natively display real second-order functions
  //////////////////////////////////////////////////////////////////////////////////////////////
  // { vtk_output_begin }
  auto vtkWriter = SubsamplingVTKWriter(
          gridView,
          refinementLevels(2));
  vtkWriter.addVertexData(
//          velocityFunction,
          bindToCoefficients(u, x),
          VTK::FieldInfo("velocity", VTK::FieldInfo::Type::vector, dim));
  vtkWriter.addVertexData(
//          pressureFunction,
          bindToCoefficients(p, x),
          VTK::FieldInfo("pressure", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.addVertexData(
          bindToCoefficients(grad(p), x),
          VTK::FieldInfo("pressure_gradient", VTK::FieldInfo::Type::vector, dim));
  vtkWriter.addVertexData(
          bindToCoefficients(div(u), x),
          VTK::FieldInfo("divergence", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.write("stokes-taylorhood-result");
  // { vtk_output_end }

 }
// Error handling
 catch (Exception& e) {
    std::cout << e.what() << std::endl;
 }
