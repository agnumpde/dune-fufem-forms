// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <vector>
#include <cmath>

#include <dune/common/bitsetvector.hh>
#include <dune/common/indices.hh>
#include <dune/common/transpose.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/raviartthomasbasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include <dune/functions/backends/istlvectorbackend.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/subspacebasis.hh>
#include <dune/functions/gridfunctions/analyticgridviewfunction.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>
#include <dune/functions/gridfunctions/facenormalgridfunction.hh>
#include <dune/functions/gridfunctions/composedgridfunction.hh>

#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/assemblers/dunefunctionsfunctionalassembler.hh>
#include <dune/fufem/backends/istlmatrixbackend.hh>
#include <dune/fufem/parallel/elementcoloring.hh>

#include <dune/fufem/forms/forms.hh>

#define DIM2 // Use a two-dimensional test, otherwise three-dimensional

using namespace Dune;



// Mark all DOFs associated to entities for which
// the boundary intersections center is marked
// by the given indicator functions.
template<class Basis, class Vector, class Indicator>
void markBoundaryDOFsByIndicator(const Basis& basis, Vector& vector, const Indicator& indicator)
{
  auto vectorBackend = Dune::Functions::istlVectorBackend(vector);
  Dune::Functions::forEachBoundaryDOF(basis, [&] (auto&& localIndex, const auto& localView, const auto& intersection) {
    if (indicator(intersection.geometry().center())>1e-8)
      vectorBackend[localView.index(localIndex)] = true;
  });
}



int main (int argc, char *argv[])
{
  std::size_t threadCount = 4;
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));

  // Set up MPI, if available
  MPIHelper::instance(argc, argv);

  ///////////////////////////////////
  //   Generate the grid
  ///////////////////////////////////

#ifdef DIM2
  const int dim = 2;
  std::array<int,dim> elements = {{50, 50}};
//  std::array<int,dim> elements = {{500, 500}};
#else
  const int dim = 3;
  std::array<int,dim> elements = {{10, 10, 10}};
#endif
  typedef YaspGrid<dim> GridType;
  FieldVector<double,dim> l(1);
  GridType grid(l,elements);

  auto gridView = grid.leafGridView();

  /////////////////////////////////////////////////////////
  //   Choose a finite element space
  /////////////////////////////////////////////////////////

  using namespace Functions::BasisFactory;

  const int k = 0;

  auto basis = makeBasis(
    gridView,
    composite(
      raviartThomas<k>(),
      lagrange<k>()
    ));

  using namespace Indices;
  auto fluxBasis = Functions::subspaceBasis(basis, _0);
  auto pressureBasis = Functions::subspaceBasis(basis, _1);


  /////////////////////////////////////////////////////////
  //   Stiffness matrix and right hand side vector
  /////////////////////////////////////////////////////////

  using MatrixType = Matrix<BCRSMatrix<FieldMatrix<double,1,1> > >;
  using VectorType = BlockVector<BlockVector<FieldVector<double,1> > >;
  using BitVectorType = BlockVector<BlockVector<FieldVector<char,1> > >;

  using Functions::istlVectorBackend;

  MatrixType stiffnessMatrix;
  VectorType rhs;

  /////////////////////////////////////////////////////////
  //  Assemble the system
  /////////////////////////////////////////////////////////

  using Domain = GridType::template Codim<0>::Geometry::GlobalCoordinate;

  auto rightHandSide = [] (const Domain& x) { return 10; };

  auto gridViewPartition = Dune::Fufem::coloredGridViewPartition(gridView);
  {
    Dune::Timer timer;
    using namespace Indices;
    using namespace ::Dune::Fufem::Forms;

    auto f = Coefficient(Functions::makeGridViewFunction(rightHandSide, gridView));

    auto sigma = trialFunction(fluxBasis);
    auto u = trialFunction(pressureBasis);
    auto tau = testFunction(fluxBasis);
    auto v = testFunction(pressureBasis);

    auto A = integrate(dot(sigma, tau) - div(tau)*u - div(sigma)*v, NonAffineFamiliyTag{});
    auto b = integrate(-f*v);

    auto operatorAssembler = Dune::Fufem::DuneFunctionsOperatorAssembler{basis, basis};
    auto matrixBackend = Dune::Fufem::istlMatrixBackend(stiffnessMatrix);

    timer.reset();
    operatorAssembler.assembleBulk(matrixBackend, A, gridViewPartition, threadCount);
    std::cout << "Assembling the problem took " << timer.elapsed() << "s" << std::endl;

    timer.reset();
    auto functionalAssembler = Dune::Fufem::DuneFunctionsFunctionalAssembler{basis};
    auto rhsBackend = Dune::Functions::istlVectorBackend(rhs);

    functionalAssembler.assembleBulk(rhsBackend, b, gridViewPartition, threadCount);
    std::cout << "Assembling the rhs took " << timer.elapsed() << "s" << std::endl;

  }

  // This marks the top and bottom boundary of the domain
  auto fluxDirichletIndicator = [&l] (const auto& x) {
    return ((x[dim-1] > l[dim-1] - 1e-8) or (x[dim-1] < 1e-8));
  };

  auto coordinate = Dune::Functions::makeAnalyticGridViewFunction([](const auto& x) { return x; }, gridView);
  auto normal = Dune::Functions::FaceNormalGridFunction(gridView);
  auto fluxDirichletValues = Dune::Functions::makeComposedGridFunction(
    [pi = std::acos(-1.0)](const auto& x, const auto& normal) {
      return (-0.05 * (1. - x[0]) * std::sin(2.*pi*x[0])) * normal;
    },
    coordinate,
    normal);

  // Mark all DOFs located in a boundary intersection marked
  // by the fluxDirichletIndicator function. If the flux
  // ansatz space also contains tangential components, this
  // approach will fail, because those are also marked.
  // For Raviart-Thomas this does not happen.
  auto isDirichlet = BitVectorType();
  istlVectorBackend(isDirichlet).resize(basis);
  isDirichlet = false;
  markBoundaryDOFsByIndicator(fluxBasis, isDirichlet, fluxDirichletIndicator);

  // Interpolate flux Dirichlet values
  interpolate(fluxBasis, rhs, fluxDirichletValues, isDirichlet);

  ////////////////////////////////////////////
  //   Modify Dirichlet rows
  ////////////////////////////////////////////

  // loop over the matrix rows
  for (size_t i=0; i<stiffnessMatrix[0][0].N(); i++)
  {
    if (isDirichlet[0][i])
    {
      // Lower right matrix block
      auto cIt    = stiffnessMatrix[0][0][i].begin();
      auto cEndIt = stiffnessMatrix[0][0][i].end();
      // loop over nonzero matrix entries in current row
      for (; cIt!=cEndIt; ++cIt)
        *cIt = (i==cIt.index()) ? 1. : 0.;

      // Lower left matrix block
      for (auto&& entry: stiffnessMatrix[0][1][i])
        entry = 0.0;
    }
  }

  ////////////////////////////
  //   Compute solution
  ////////////////////////////

  // Start from the rhs vector; that way the Dirichlet entries are already correct
  VectorType x = rhs;

  // Technicality:  turn the matrix into a linear operator
  MatrixAdapter<MatrixType,VectorType,VectorType> op(stiffnessMatrix);

  // Fancy (but only) way to not have a preconditioner at all
  Richardson<VectorType,VectorType> preconditioner(1.0);

  // Preconditioned GMRES / BiCGSTAB solver
  //RestartedGMResSolver<VectorType> solver (op, preconditioner, 1e-6, 1000, 10000, 2);
  BiCGSTABSolver<VectorType> solver(op, preconditioner, 1e-6, 4000, 2);

  // Object storing some statistics about the solving process
  InverseOperatorResult statistics;

  // Solve!
  solver.apply(x, rhs, statistics);

  ////////////////////////////////////////////////////////////////////////////
  //  Make a discrete function from the FE basis and the coefficient vector
  ////////////////////////////////////////////////////////////////////////////

  using FluxRange = FieldVector<double,dim>;
  using PressureRange = double;

  auto fluxFunction = Functions::makeDiscreteGlobalBasisFunction<FluxRange>(fluxBasis, x);
  auto pressureFunction = Functions::makeDiscreteGlobalBasisFunction<PressureRange>(pressureBasis, x);

  //////////////////////////////////////////////////////////////////////////////////////////////
  //  Write result to VTK file
  //////////////////////////////////////////////////////////////////////////////////////////////
  auto vtkWriter = SubsamplingVTKWriter(gridView, Dune::refinementLevels(0));
  vtkWriter.addVertexData(fluxFunction, VTK::FieldInfo("flux", VTK::FieldInfo::Type::vector, dim));
  vtkWriter.addVertexData(pressureFunction, VTK::FieldInfo("pressure", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.write("poisson-mfem-result");
}
